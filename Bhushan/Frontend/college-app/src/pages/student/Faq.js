import StudentHome from '../../components/home/studenthome'
const styles = {
    div: {
        float: 'right',
        width: '80%',

    }
}
const Faq = () => {
    return (
        <div className="personalDetails">
            <div>
                <StudentHome />
            </div>
            <div className="row">
                
                <div className="col">
                    <div class="accordion accordion-flush" id="accordionFlushExample" style={styles.div}>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                    How Online Admission System Ease Institute Admission?
                                </button>
                            </h2>
                            <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body"><p>Online admission software eliminates the hassle of standing in long queues, enables immediate online transactions, and provides real time stats of admission-related tasks by streamlining the entire student admission procedure.</p></div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                    What Is College Management Software?
                                </button>
                            </h2>
                            <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">College Management Software is a comprehensive educational ERP solution to streamline the work processes in colleges & boost the overall productivity.</div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                    What Is A Student portal?
                                </button>
                            </h2>
                            <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">Student portal is a vast database of student related information that assists educational institutions to manage every single detail about students from a centralized location in a systematic, error-free, organized and cost-effective manner. From streamlining & managing functions like student registration, admission, billing, attendance information, to health records, the inbuilt functionalities of student management software simplify administrative tasks & ensure a systematic work ecosystem in educational campuses.

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
    )
}

export default Faq;