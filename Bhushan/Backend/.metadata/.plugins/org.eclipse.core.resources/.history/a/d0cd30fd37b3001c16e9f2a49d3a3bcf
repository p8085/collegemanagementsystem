package com.sunbeam.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.component.PlacementComponent;
import com.sunbeam.component.Response;
import com.sunbeam.dto.placement.CompanyDTO;
import com.sunbeam.dto.placement.CompanyDetailsDTO;
import com.sunbeam.dto.placement.CompanyEligibilityDTO;
import com.sunbeam.dto.placement.CompanyListDTO;
import com.sunbeam.entities.Placement;
import com.sunbeam.service.PlacementServiceImpl;

@RestController
public class PlacementControllerImpl {

	@Autowired
	private PlacementServiceImpl placementService;

	@Autowired
	private PlacementComponent placementComponent;

	@GetMapping("/companydetails/{id}")
	public ResponseEntity<?> findByCompanyId(@PathVariable("id") int companyId) {
		CompanyDetailsDTO companyDetails = placementService.findByCompanyId(companyId);
		if (companyDetails == null)
			return Response.error("Company Not Found");
		return Response.success(companyDetails);
	}

	@GetMapping("/companydetails/byname")
	public ResponseEntity<?> findByCompanyName(@RequestBody Placement placement) {
		CompanyDetailsDTO companyDetails = placementService.findByCompanyName(placement);
		if (companyDetails == null)
			return Response.error("Company Not Found");
		return Response.success(companyDetails);
	}

	@GetMapping("/companydetails/bytype")
	public ResponseEntity<?> findByCompanyType(@RequestBody Placement placement) {
		List<CompanyDetailsDTO> companyDetails = placementService.findByCompanyType(placement);
		if (companyDetails == null)
			return Response.error("Company Not Found");
		return Response.success(companyDetails);
	}

	@PostMapping("/addcompany")
	public ResponseEntity<?> addCompany(@RequestBody CompanyDTO addCompanyDTO) {
		Map<String, Object> company = placementService.addCompany(addCompanyDTO);
		if (company == null)
			return Response.error("Company Can Not Be Added");
		return Response.success(company);
	}

	@GetMapping("/companyeligibilitycriteria")
	public ResponseEntity<?> showCompanyEligibiltyCriteria(@RequestBody Placement placement) {
		CompanyEligibilityDTO companyEligibilityDto = placementService.showCompanyEligibiltyCriteria(placement);
		if (companyEligibilityDto == null)
			return Response.error("Company Not Found");
		return Response.success(companyEligibilityDto);
	}

	@GetMapping("/allcompanylist")
	public ResponseEntity<?> findAllCompanies() {
		List<CompanyListDTO> companyList = placementService.findAllCompanies();
		if (companyList == null)
			return Response.error("Company List Not Availabe Right Now !!! ");
		return Response.success(companyList);
	}

	@PutMapping("/companydetails/{id}")
	public ResponseEntity<?> updateCompanyDetails(@PathVariable("id") int companyId,
			@RequestBody CompanyDetailsDTO companyDetailsDto) {
		if (placementComponent.isJASONObjectIsEmpty(companyDetailsDto))
			return Response.error("No Data For Update Company Details");
		Map<String, Object> newCompanyDetails = placementService.updateCompanyDetails(companyId, companyDetailsDto);
		if (newCompanyDetails == null)
			return Response.error("Company Not Found");
		return Response.success(newCompanyDetails);
	}

	@PutMapping("/companyeligibilitycriteria/{id}")
	public ResponseEntity<?> updateCompanyEligibilityCriteria(@PathVariable("id") int companyId,
			@RequestBody CompanyEligibilityDTO companyEligibilityDto) {
		if (placementComponent.isJASONObjectIsEmpty(companyEligibilityDto))
			return Response.error("No Data For Update Company Eligibility Criteria");
		Map<String, Object> newCompanyEligibilityCriteria = placementService.updateCompanyEligibiltyCriteria(companyId,
				companyEligibilityDto);
		if (newCompanyEligibilityCriteria == null)
			return Response.error("Company Not Found");
		return Response.success(newCompanyEligibilityCriteria);
	}

	@DeleteMapping("/company/{id}")
	public ResponseEntity<?> deleteCompany(@PathVariable("id") int companyId) {
		Map<String, Object> deletedCompany = placementService.deleteCompany(companyId);
		if (deletedCompany == null)
			return Response.error("Unable To Delete Company");
		return Response.success(deletedCompany);
	}
}
