import StudentSignin from '../signin'

const StudentLogout=()=>
{
    sessionStorage.removeItem('id')
    sessionStorage.removeItem('firstName')
    sessionStorage.removeItem('lastName')
    sessionStorage.removeItem('loginStatus')
    return (
        <div className="Logout">
            <StudentSignin />
        </div>
    )
}

export default StudentLogout;