import axios from "axios";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { URL } from "../../config";
import PlacementHome from '../../components/home/placementhome'

const AddCompany = () => {
  const [companyName, setCompanyName] = useState("");
  const [companyType, setCompanyType] = useState("");
  const [sscScore, setSscScore] = useState("");
  const [hscScore, setHscScore] = useState("");
  const [diplomaScore, setDiplomaScore] = useState("");
  const [degreeScore, setDegreeScore] = useState("");
  const [vacancies, setVacancies] = useState("");
  const [companyCtc, setCompanyCtc] = useState("");
    
  const navigateTo = useNavigate();

  const save = () => {
    if (companyName.length === 0) {
      toast.warning("company Name field can not be empty");
    } else if (companyType.length === 0) {
      toast.warning("company Type field can not be empty");
    } else if (sscScore.length === 0) {
      toast.warning("ssc Score field can not be empty");
    } else if (hscScore.length === 0) {
      toast.warning("hsc Score field can not be empty");
    } else if (diplomaScore.length === 0) {
      toast.warning("diploma Score field can not be empty");
    } else if (degreeScore.length === 0) {
      toast.warning("degree Score field can not be empty");
    } else if (vacancies.length === 0) {
      toast.warning("vacancies field can not be empty");
    } else if (companyCtc.length === 0) {
      toast.warning("company Ctc field can not be empty");
    } else {
      const body = {
        companyName,
        companyType,
        sscScore,
        hscScore,
        diplomaScore,
        degreeScore,
        vacancies,
        companyCtc,
      };

      const url = `${URL}/addcompany`;
      axios.post(url, body).then((response) => {
        const result = response.data;
        if (result["status"] == "success") {
          toast.success("New Company Added Successfully");
          navigateTo("/placementhome");
        } else {
          toast.error(result["error"]);
        }
      });
    }
  };

  const cancel = () => {
    navigateTo("/placementhome");
  };

  return (
    <div>
    <div>
        <PlacementHome/>
    </div>
      <div className="container">
        <div className="form">
          <div className="row">
            <div className="col-2">Menus</div>
            <div className="col">
              <div className="row">
                <div className="col"></div>
                <div className="col">
                  <h2>Add New Company</h2>
                </div>
                <div className="col"></div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setCompanyName(e.target.value);
                      }}
                      type="text"
                      className="form-control"
                      placeholder="name@example.com"
                    />
                    <label>Company Name</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setCompanyType(e.target.value);
                      }}
                      type="text"
                      className="form-control"
                      placeholder="name@example.com"
                    />
                    <label>Company Type</label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setSscScore(e.target.value);
                      }}
                      type="number"
                      className="form-control"
                      placeholder="name@example.com"
                    />
                    <label>SSC Score</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setHscScore(e.target.value);
                      }}
                      type="number"
                      className="form-control"
                      placeholder="Password"
                    />
                    <label>HSC Score</label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setDiplomaScore(e.target.value);
                      }}
                      type="number"
                      className="form-control"
                      placeholder="Password"
                    />
                    <label>DIPLOMA Score</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setDegreeScore(e.target.value);
                      }}
                      type="number"
                      className="form-control"
                      placeholder="Password"
                    />
                    <label>DEGREE Score</label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setVacancies(e.target.value);
                      }}
                      type="number"
                      className="form-control"
                      placeholder="Password"
                    />
                    <label>Vacancies</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setCompanyCtc(e.target.value);
                      }}
                      type="number"
                      className="form-control"
                      placeholder="Password"
                    />
                    <label>Company CTC</label>
                  </div>
                </div>
             </div>
            </div>
          </div>
        </div>
        <div className="d-grid gap-2 d-md-flex justify-content-md-end">
          <button
            onClick={save}
            className="btn btn-primary me-md-2"
            type="button"
          >
            Save
          </button>
          <button onClick={cancel} type="button" className="btn btn-danger">
            Cancel
          </button>
        </div>
        </div>
        </div>
  );
};

export default AddCompany;
