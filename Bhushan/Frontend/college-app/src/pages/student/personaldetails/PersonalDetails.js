import axios from 'axios';
import { useNavigate } from 'react-router';
import { toast, ToastContainer } from 'react-toastify';
import { URL } from '../../../config'
import { useState } from 'react';
import React, { useEffect } from 'react';
import StudentHome from '../../../components/home/studenthome'

const styles={
   table:{
    marginTop: '10px',
    marginBottom: '10px',
    marginLeft:'auto',
    left:'auto',
    marginRight:'auto',
    padding:'5px',
    width:'60%',
    border:' 1px solid black',

       
   },
   row: {
    borderColor: 'inherit',
    borderStyle: 'solid',
    borderWidth: '10',
    marginLeft:'10%',
    marginRight:'10%',
    paddingLeft: '4em',
},
div:{
    //textAlign : 'right',
    // width:'100%',
    // marginLeft:'auto',
    // marginRight:'0'
}

}

const StudentPersonalDetails=()=>{
    const { enrollmentNo, firstName, lastName } = sessionStorage
    const navigate = useNavigate()
    const [ studentDetails, setStudentDetails ] = useState([])
    
    const searchStudent=()=>{
        console.log({enrollmentNo})
        const url = `${URL}/student/${enrollmentNo}`
        console.log({url})
       axios.get(url).then((response)=>{
            const result=response.data
            console.log('inside axios after  reponse')
            if(result['status'] === 'success'){
                setStudentDetails(result['data'])
            }else{
                toast.error(result['error'])

            }
        })
    }
    useEffect(()=>{
        searchStudent()
        console.log('inside searchstudent')
    },[])
  
    console.log({studentDetails})

    return (
        <div className="personalDetails">
            <div>
                 <StudentHome />
            </div>
            <div style={styles.table}>
            <table className="table table-striped">
                <td></td>
                
                
                <tbody>
                    
                    <tr style={styles.row}>
                        <th scope="row">1</th>
                        <td border='1px solid black' align='left'>First Name </td> 
                        <td border='1px solid black' align='left'>{studentDetails.firstName}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">2</th>
                        <td align='left'>Middle Name </td> 
                        <td align='left'>{studentDetails.middleName}</td>
                   </tr>
                    <tr style={styles.row}>
                        <th scope="row">3</th>
                        <td align='left'>Last Name </td> 
                        <td align='left'>{studentDetails.lastName}</td>
                    </tr> 
                    <tr style={styles.row}>
                        <th scope="row">4</th>
                        <td align='left'>Date of Birth</td>
                        <td align='left'>{studentDetails.dob}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">5</th>  
                        <td align='left'>Email </td>
                        <td align='left'>{studentDetails.email}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">6</th>
                        <td align='left'>Password </td>
                        <td align='left'>{studentDetails.password}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">7</th>
                        <td align='left'>Contact No </td>
                        <td align='left'>{studentDetails.contactNo}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">8</th>
                        <td align='left'>Address Line1 </td>
                        <td align='left'>{studentDetails.addressLine1}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">9</th>
                        <td align='left'>Address Line2 </td>
                        <td align='left'>{studentDetails.addressLine2}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">10</th>
                        <td align='left'>Zip id</td>
                        <td align='left'>{studentDetails.zipId}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">11</th>
                        <td align='left'>Admission Type </td>
                        <td align='left'>{studentDetails.admissionType}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">12</th>
                        <td align='left'>Current Semester </td>
                        <td align='left'>{studentDetails.currentSem}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">13</th>
                        <td align='left'>Department Id </td>
                        <td align='left'>{studentDetails.departmentId}</td>
                    </tr>
                    
                </tbody>
            </table>
                
            </div>
            
            <div className='text-right'>
                            <button onClick={() => {
                                navigate('/edit-details',{ state : { studentDetails : studentDetails } })
                            }} 
                            className="btn btn-secondary btn-sm" type="button"
                            >
                                Edit
                            </button>
                            
                            
                </div>

        </div>
    )
}

export default StudentPersonalDetails; 