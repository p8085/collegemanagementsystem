import logo from './logo.svg';
import GuestSignin from './pages/guest/signin'
import GuestSignup from './pages/guest/signup'
import StudentSignin from './pages/student/signin'
import GuestHome from './components/home/guesthome'
import StaffHome from './components/home/staffhome'
import PlacementHome from './components/home/placementhome'
import StudentHome from './components/home/studenthome'
import AddEmployee from './pages/staffmember/add_staffmember'
import AddDepartment from './pages/department/add_department'
import Add from './pages/admin/admin_options/add'
import StaffMemberDetails from './components/staffMemberDetails'
import { BrowserRouter, Route, Routes, Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import './App.css';
import FillApplicationForm from './pages/applicationform/fillapplicationform';
import GuestPersonalDetails from './pages/guest/personalinfo/personalDetails'
import StudentPersonalDetails from './pages/student/personaldetails/PersonalDetails'
import AcademicDetails from './pages/student/academicdetails/AcademicDetails'
import ChangePassword from './pages/student/ChangePassword'
import StudentLogout from './pages/student/signout/Logout'
import Faq from './pages/student/Faq'
import EditStudentDetails from './pages/student/personaldetails/EditDetails'
import EditAcademicDetails from './pages/student/academicdetails/EditAcademicDetails'
import AddAcademicDetails from './pages/student/academicdetails/AddAcademicDetails'
import GetAllStudents from './pages/student/GetAllStudents'
import DisplayStudents from './pages/student/DisplayStudents'
import FacultyPaper from './pages/facultypaper/facultyPaper'
import DeleteStaffmember from './pages/staffmember/delete_staffmember/delete'
import EditStaffMember from './pages/staffmember/edit_staffmember/editStaffMembers'
import AddSubject from './pages/subject/add_subject'
import DepartmentDetails from './pages/department/add_department'
import AddCompany from './pages/placement/addcompany'

function App() {
  return (
    <div className="container">
      <BrowserRouter>
        <ul>
         <li>
            <Link to="/guest/signup">Guest Signup</Link>
          </li>
        <li>
            <Link to="/guest/signin">Guest Signin</Link>
          </li>
          <li>
            <Link to="/student/signin">Student Signin</Link>
          </li>
          <li>
            <Link to="/admin/add/employee">Add employee</Link>
          </li>
          <li>
            <Link to="/admin/add">Add</Link>
          </li>
          <li>
            <Link to="/admin/allStaffMembers">get all employee</Link>
          </li>
          <li>
            <Link to="/placementhome">Placement Cell</Link>
          </li>
        </ul>
        <Routes>
          <Route path="/" element={<Home/>} />  
          <Route path="/guest/signin" element={<GuestSignin />} />
           <Route path="/student/signin" element={<StudentSignin />} />
          <Route path="/guest/signup" element={<GuestSignup />} />
          <Route path="/guesthome" element={<GuestHome />} />
          <Route path="/staffhome" element={<StaffHome/>} />
           <Route path="/studenthome" element={<StudentHome/>} />
          <Route path="/admin/add/employee" element={<AddEmployee />} />
          <Route path="/admin/add/department" element={<AddDepartment />} />
          <Route
            path="/admin/allStaffMembers"
            element={<StaffMemberDetails />}
          />
          <Route path="/admin/add" element={<Add />} />
          <Route path="/guest/fillapplicationform" element={<FillApplicationForm/>}/>
          <Route path="/guest/personalDetails" element={<GuestPersonalDetails/>}/>
           <Route path={`/admin/delete/employee`} element={<DeleteStaffmember />} />
          <Route path={`/admin/FacultyPaper/:id`} element={<FacultyPaper />} />
          <Route path={"/admin/edit-employee"} element={<EditStaffMember />} />
          <Route path={"/admin/add/subject"} element={<AddSubject />} />
           <Route path={"/departments/all"} element={<DepartmentDetails />} />

           <Route path="/student/personalDetails" element={<StudentPersonalDetails />}/>
      <Route path="/stduent/academicDetails" element={<AcademicDetails />}/>
      <Route path="/stduent/changePassword" element={<ChangePassword />}/>
      <Route path="/stduent/logout" element={<StudentLogout />}/>
      <Route path="/student/faq" element={<Faq />}/>
      <Route path="/stduent/edit-details" element={<EditStudentDetails />}/>
      <Route path="/stduent/edit-academicdetails" element={<EditAcademicDetails />}/>
      <Route path="/stduent/add-academicdetails" element={<AddAcademicDetails />}/>
      <Route path="/stduent/getAllStudents" element={<GetAllStudents />}/>
      <Route path="/stduent/displayStudents" element={<DisplayStudents />}/>

      <Route path="/admin/addcompany" element={<AddCompany/>}/>
      <Route path="/placementhome" element={<PlacementHome/>}/>

        </Routes>
      </BrowserRouter>
      <ToastContainer theme="colored" />
    </div>
  )
}

export default App;
