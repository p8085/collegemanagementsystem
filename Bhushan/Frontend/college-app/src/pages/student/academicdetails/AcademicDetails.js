import axios from 'axios';
import { useNavigate } from 'react-router';
import { toast, ToastContainer } from 'react-toastify';
import { URL } from '../../../config'
import { useState } from 'react';
import React, { useEffect } from 'react';
import StudentHome from '../../../components/home/studenthome'


const styles={
    table:{
     marginTop: '10px',
     marginBottom: '10px',
     marginLeft:'auto',
     marginRight:'auto',
     padding:'5px',
     width:'65%',
     border:' 1px solid black',
 
        
    },
    row: {
     borderColor: 'inherit',
     borderStyle: 'solid',
     borderWidth: '10',
     marginLeft:'10%',
     marginRight:'10%',
     paddingLeft: '4em',
 },
 div:{
     //textAlign : 'right',
     // width:'100%',
     // marginLeft:'auto',
     // marginRight:'0'
 }
 
 }
 
const AcademicDetails=()=>
{
    const { enrollmentNo, firstName, lastName } = sessionStorage
    const navigate = useNavigate()
    const [ academicRecords, setAcademicRecords ] = useState([])
    
    const searchRecords=()=>{
        console.log({enrollmentNo})
        const url = `${URL}/student/getAcademicRecords/${enrollmentNo}`
        console.log({url})
        axios.get(url).then((response)=>{
            const result=response.data
            console.log('inside axios after  reponse')
            if(result['status'] === 'success'){
                setAcademicRecords(result['data'])
            }else{
                toast.error(result['error'])

            }
        })
    }
    useEffect(()=>{
        searchRecords()
        console.log('inside searchRecords')
    },[])
  
    console.log({academicRecords})

    return (
        <div className="academicDetails">
            <div>
                 <StudentHome />
            </div>
            <div className="row">
                <div className="col-2"></div>
                <div className="col">
                <div style={styles.table}>
            <table className="table table-striped">
                <td></td>
                
                
                <tbody>
                    
                    <tr style={styles.row}>
                        <th scope="row">1</th>
                        <td border='1px solid black' align='left'>SSC Score</td> 
                        <td border='1px solid black' align='left'>{academicRecords.sscScore}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">2</th>
                        <td align='left'>HSC Score </td> 
                        <td align='left'>{academicRecords.hscScore}</td>
                   </tr>
                    <tr style={styles.row}>
                        <th scope="row">3</th>
                        <td align='left'>Diploma Score</td> 
                        <td align='left'>{academicRecords.diplomaScore}</td>
                    </tr> 
                    <tr style={styles.row}>
                        <th scope="row">4</th>
                        <td align='left'>first Semester</td>
                        <td align='left'>{academicRecords.firstSem}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">5</th>  
                        <td align='left'>Second Semester </td>
                        <td align='left'>{academicRecords.secondSem}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">6</th>
                        <td align='left'>Third Semester </td>
                        <td align='left'>{academicRecords.thirdSem}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">7</th>
                        <td align='left'>Fourth Semester </td>
                        <td align='left'>{academicRecords.fourthSem}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">8</th>
                        <td align='left'>Fifth Semester </td>
                        <td align='left'>{academicRecords.fifthSem}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">9</th>
                        <td align='left'>Sixth Semester </td>
                        <td align='left'>{academicRecords.sixthSem}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">10</th>
                        <td align='left'>Seventh Semester</td>
                        <td align='left'>{academicRecords.seventhSem}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">11</th>
                        <td align='left'>Eighth Semester </td>
                        <td align='left'>{academicRecords.eighthSem}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">12</th>
                        <td align='left'>Grade </td>
                        <td align='left'>{academicRecords.grade}</td>
                    </tr>
                                       
                </tbody>
            </table>
                
            </div>
                </div>
            </div>
            
            <div className='d-grid gap-2 d-md-block'>
                            <button onClick={() => {
                                navigate('/edit-academicdetails',{ state : { academicRecords : academicRecords } })
                            }} 
                            className="btn btn-secondary btn-sm" type="button"
                            >
                                Edit
                            </button>
                            <span></span>
                            <button onClick={() => {
                                navigate('/add-academicdetails')
                            }} 
                            className="btn btn-secondary btn-sm" type="button"
                            >
                                Add
                            </button>
                            
                            
                </div>



        </div>
    )
}

export default AcademicDetails;