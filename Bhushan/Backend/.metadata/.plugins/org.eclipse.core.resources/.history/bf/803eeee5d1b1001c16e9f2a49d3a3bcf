package com.sunbeam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dto.CompanyDetailsDTO;
import com.sunbeam.dto.Response;
import com.sunbeam.entities.Placement;
import com.sunbeam.service.PlacementServiceImpl;

@RestController
public class PlacementControllerImpl {
	
	@Autowired
	private PlacementServiceImpl placementService;
	
	@GetMapping("/companydetails/{id}")
	public ResponseEntity<?> findByCompanyId(@PathVariable("id") int companyId){
		CompanyDetailsDTO companyDetails = placementService.findByCompanyId(companyId);
		if(companyDetails==null)
			return Response.error("Company Not Found");
		return Response.success(companyDetails);
	}
	
	@GetMapping("/companydetails/byname")
	public ResponseEntity<?> findByCompanyName(@RequestBody Placement placement){
		CompanyDetailsDTO companyDetails = placementService.findByCompanyName(placement);
		if(companyDetails==null)
			return Response.error("Company Not Found");
		return Response.success(companyDetails);
	}
	
	@GetMapping("/companydetails/bytype")
	public ResponseEntity<?> findByCompanyType(@RequestBody Placement placement){
		List<CompanyDetailsDTO> companyDetails = placementService.findByCompanyType(placement);
		if(companyDetails==null)
			return Response.error("Company Not Found");
		return Response.success(companyDetails);
	}
	
	@PostMapping("/addcompany")
	public ResponseEntity<?> addCompany(@RequestBody Placement placement){
		CompanyDetailsDTO companyDetails = placementService.addCompany(placement);
		if(companyDetails==null)
			return Response.error("Company Not Found");
		return Response.success(companyDetails);
	}
}
