package com.sunbeam.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunbeam.dao.PlacementDao;
import com.sunbeam.dto.CompanyDTO;
import com.sunbeam.dto.CompanyDetailsDTO;
import com.sunbeam.dto.CompanyEligibilityDTO;
import com.sunbeam.dto.CompanyListDTO;
import com.sunbeam.dto.EntityDTOConverter;
import com.sunbeam.entities.Placement;

@Transactional
@Service
public class PlacementServiceImpl {

	@Autowired
	private PlacementDao placementDao;

	@Autowired
	private EntityDTOConverter converter;

	public CompanyDetailsDTO findByCompanyId(int companyId) {
		Placement companyDetails = placementDao.findByCompanyId(companyId);
		if (companyDetails == null)
			return null;
		return converter.toPlacementDto(companyDetails);
	}

	public CompanyDetailsDTO findByCompanyName(Placement placement) {
		Placement companyDetails = placementDao.findByCompanyName(placement.getCompanyName());
		if (companyDetails == null)
			return null;
		return converter.toPlacementDto(companyDetails);
	}

	public List<CompanyDetailsDTO> findByCompanyType(Placement placement) {
		List<Placement> listByCompanyType = placementDao.findByCompanyType(placement.getCompanyType());
		if (listByCompanyType.isEmpty())
			return null;
		return listByCompanyType.stream().map(companyDetails -> converter.toListOfPlacementDTO(companyDetails))
				.collect(Collectors.toList());
	}

	public Map<String, Object> addCompany(CompanyDTO addCompanyDTO) {
		Placement placement = converter.toCompanyEntity(addCompanyDTO);
		placement = placementDao.save(placement);
		return Collections.singletonMap("insertedId", placement.getCompanyId());
	}

	public CompanyEligibilityDTO showCompanyEligibiltyCriteria(Placement placement) {
		Placement companyEligibiityCriteria = placementDao.findByCompanyName(placement.getCompanyName());
		if (companyEligibiityCriteria == null)
			return null;
		return converter.toCompanyEligibiityCriteriaDto(companyEligibiityCriteria);
	}

	public List<CompanyListDTO> findAllCompanies() {
		List<Placement> companyList = placementDao.findAll();
		if (companyList.isEmpty())
			return null;
		return companyList.stream().map(company -> converter.toCompanyListDto(company)).collect(Collectors.toList());
	}

	public Map<String, Object> updateCompanyDetails(int companyId, CompanyDetailsDTO companyDetailsDto) {
		if (placementDao.existsById(companyId)) {
			Placement companyDetails = placementDao.getById(companyId);
			companyDetailsDto.setCompanyId(companyId);
			Placement newCompanyDetails = converter.toCompanyDetailsEntity(companyDetailsDto, companyDetails);
			newCompanyDetails = placementDao.save(newCompanyDetails);
			return Collections.singletonMap("Company Details Updated Of companyId : ",
					newCompanyDetails.getCompanyId());
		}
		return null;

	}

	public Map<String, Object> updateCompanyEligibiltyCriteria(int companyId,
			CompanyEligibilityDTO companyEligibilityDto) {
		if (placementDao.existsById(companyId)) {
			Placement companyEligibilityCriteria = placementDao.getById(companyId);
			companyEligibilityDto.setCompanyId(companyId);
			Placement newCompanyEligibilityCriteria = converter.toCompanyEligibilityEntity(companyEligibilityDto,
					companyEligibilityCriteria);
			newCompanyEligibilityCriteria = placementDao.save(newCompanyEligibilityCriteria);
			return Collections.singletonMap("Company Eligibility Criteria Updated Of companyName : ",
					newCompanyEligibilityCriteria.getCompanyName());
		}
		return null;

	}
}
