import { useEffect, useState } from 'react'
import axios from 'axios'
import { URL } from '../../config'
import DisplayStudents from './DisplayStudents'

const GetAllStudents=()=>{

    const [ getStudents,setStudents ] = useState([])
    const allStudents=()=>{
        const url=`${URL}/student/findAllStudents/`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] == 'success') {
              setStudents(result['data'])
            }
          })
    }
    useEffect(()=>{
        allStudents()
    },[])
    console.log(getStudents)
    return(
        <div>   
            <div>
            {getStudents.map((stud) => {
                return <DisplayStudents students={stud}/>
            })}
            </div>
            <div>
                <a href='addStudent'>Add Student</a>
            </div>
        </div>
    )
}

export default GetAllStudents