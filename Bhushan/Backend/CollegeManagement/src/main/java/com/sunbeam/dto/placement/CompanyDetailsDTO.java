package com.sunbeam.dto.placement;

import org.springframework.stereotype.Component;

@Component
public class CompanyDetailsDTO {
	private int companyId;
	private String companyName;
	private String companyType;
	private int vacancies;
	private double companyCtc;

	public CompanyDetailsDTO() {
		super();
	}

	public CompanyDetailsDTO(int vacancies, double companyCtc) {
		super();
		this.vacancies = vacancies;
		this.companyCtc = companyCtc;
	}

	public CompanyDetailsDTO(double companyCtc) {
		super();
		this.companyCtc = companyCtc;
	}

	public CompanyDetailsDTO(int vacancies) {
		super();
		this.vacancies = vacancies;
	}

	public CompanyDetailsDTO(String companyType) {
		super();
		this.companyType = companyType;
	}

	public CompanyDetailsDTO(int companyId, String companyName, String companyType, int vacancies, double companyCtc) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.companyType = companyType;
		this.vacancies = vacancies;
		this.companyCtc = companyCtc;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public int getVacancies() {
		return vacancies;
	}

	public void setVacancies(int vacancies) {
		this.vacancies = vacancies;
	}

	public double getCompanyCtc() {
		return companyCtc;
	}

	public void setCompanyCtc(double companyCtc) {
		this.companyCtc = companyCtc;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	@Override
	public String toString() {
		return "CompanyDetailsDTO [companyId=" + companyId + ", companyName=" + companyName + ", companyType="
				+ companyType + ", vacancies=" + vacancies + ", companyCtc=" + companyCtc + "]";
	}

}
