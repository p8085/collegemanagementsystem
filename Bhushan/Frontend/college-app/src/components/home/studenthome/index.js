import React, { useEffect } from 'react';
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { studentSidebar } from './studentsidebar'
import "../cssfile/index.css"
import axios from 'axios';
import { useNavigate } from 'react-router';
import { toast } from 'react-toastify';



const StudentHome=()=>{
    const [ sidebar,setSidebar ] = useState(false)
    const { enrollmentNo, firstName, lastName } = sessionStorage
    const navigate = useNavigate()
    

    


    const showSidebar = () => setSidebar(!sidebar)
    return( 
        <div>
            <div className="navbar">
                <Link to='#' className='menu-bars'>
                    <FaIcons.FaBars onClick={showSidebar}></FaIcons.FaBars>
                </Link>
                <h3 className='header'>Student Portal</h3> 
                <p className='username'> Welcome {sessionStorage.firstName} {sessionStorage.lastName}</p>
            </div>
            <nav className={sidebar ? 'nav-menu active' : 'nav-menu active'}>
                <ul className='nav-menu-items' onClick={showSidebar}>
                    <li className='navbar-toggle'>
                        <Link to='#' className='menu-bars'>
                            <AiIcons.AiOutlineClose />
                        </Link>
                    </li>
                    {studentSidebar.map((item,index)=>{
                        return(
                            <li key={index} className={item.cName}>
                                <Link to={item.path}>
                                    {item.icon}
                                    <spam>{item.title}</spam>
                                </Link>
                            </li>
                        )
                    })}
                </ul>
            </nav>
            <div>
            
                    <div>
                        <h4 className='Welcomeline'>
                           
                        </h4>
                    </div>
            
            </div>
        </div>
    )
}


export default StudentHome
