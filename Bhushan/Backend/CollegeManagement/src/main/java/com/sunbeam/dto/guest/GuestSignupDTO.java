package com.sunbeam.dto.guest;

import org.springframework.stereotype.Component;

@Component
public class GuestSignupDTO {

	private int registrationNo;
	private String email;
	private String password;
	private String firstName;
	private String lastName;

	public GuestSignupDTO() {
		super();
	}

	public GuestSignupDTO(int registrationNo, String email, String password, String firstName, String lastName) {
		super();
		this.registrationNo = registrationNo;
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public int getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(int registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "GuestSignUpDTO [registrationNo=" + registrationNo + ", email=" + email + ", password=" + password
				+ ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}

}
