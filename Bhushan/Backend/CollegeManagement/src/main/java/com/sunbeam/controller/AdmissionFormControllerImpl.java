package com.sunbeam.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.component.Response;
import com.sunbeam.dto.admissionform.AdmissionFormDTO;
import com.sunbeam.service.AdmissionFormServiceImpl;

@RestController
public class AdmissionFormControllerImpl {
	
	@Autowired
	private AdmissionFormServiceImpl admissionFormService;
	
	@PostMapping("/filladmissionform")
	public ResponseEntity<?> fillAdmissionForm(@RequestBody AdmissionFormDTO AdmissionFormDTO) {
		Map<String, Object> admissionForm = admissionFormService.fillAdmissionForm(AdmissionFormDTO);
		if (admissionForm == null)
			return Response.error("Form Can Not Be Submitted.....Please Try Again !!!");
		return Response.success(admissionForm);
	}
}
