package com.sunbeam.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="staff")
public class Staff {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name="employee_id")
	private int employeeId;
	@Column(name="first_name")
	private String firstName;
	@Column(name="middle_name")
	private String middleName;
	@Column(name="last_name")
	private String lastName;
	@Column(name="email")
	private String email;
	@Column(name="password")
	private String password;
	@Temporal(TemporalType.DATE)
	@Column(name="dob")
	private Date dob;
	@Column(name="work_exp")
	private Double workExp;
	@Column(name="published_paper")
	private int publishedPaper;
	@Temporal(TemporalType.DATE)
	@Column(name="hire_dat")
	private Date hireDate;
	@Column(name="contact_no")
	private String contactNo;
	@Column(name="gender")
	private String gender;
	@Column(name="salary")
	private int salary;
	private String imageUrl;
	@ManyToOne
	private Department department_id;
	@ManyToMany
	private List<Subject> subjects;
	
	public Staff() {
		super();
	}

	public Staff(int employeeId, String firstName, String middleName, String lastName, String email, String password,
			Date dob, Double workExp, int publishedPaper, Date hireDate, String contactNo, String gender, int salary,
			Department department_id, List<Subject> subjects) {
		super();
		this.employeeId = employeeId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.dob = dob;
		this.workExp = workExp;
		this.publishedPaper = publishedPaper;
		this.hireDate = hireDate;
		this.contactNo = contactNo;
		this.gender = gender;
		this.salary = salary;
		this.department_id = department_id;
		this.subjects = subjects;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Double getWorkExp() {
		return workExp;
	}

	public void setWorkExp(Double workExp) {
		this.workExp = workExp;
	}

	public int getPublishedPaper() {
		return publishedPaper;
	}

	public void setPublishedPaper(int publishedPaper) {
		this.publishedPaper = publishedPaper;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public Department getDepartment_id() {
		return department_id;
	}

	public void setDepartment_id(Department department_id) {
		this.department_id = department_id;
	}

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	@Override
	public String toString() {
		return "Staff [employeeId=" + employeeId + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", email=" + email + ", password=" + password + ", dob=" + dob
				+ ", workExp=" + workExp + ", publishedPaper=" + publishedPaper + ", hireDate=" + hireDate
				+ ", contactNo=" + contactNo + ", gender=" + gender + ", salary=" + salary + ", department_id="
				+ department_id + ", subjects=" + subjects + "]";
	}
	
	
	
}
