import axios from 'axios';
import { useNavigate } from 'react-router';
import { toast, ToastContainer } from 'react-toastify';
import { URL } from '../../../config'
import { useState } from 'react';
import React, { useEffect } from 'react';
import GuestHome from '../../../components/home/guesthome'

const styles={
   table:{
    marginTop: '10px',
    marginBottom: '10px',
    marginLeft:'auto',
    left:'auto',
    marginRight:'auto',
    padding:'5px',
    width:'60%',
    border:' 1px solid black',

       
   },
   row: {
    borderColor: 'inherit',
    borderStyle: 'solid',
    borderWidth: '10',
    marginLeft:'10%',
    marginRight:'10%',
    paddingLeft: '4em',
},
div:{
    //textAlign : 'right',
    // width:'100%',
    // marginLeft:'auto',
    // marginRight:'0'
}

}

const GuestPersonalDetails=()=>{
    const { registrationNo , firstName, lastName , applicationStatus } = sessionStorage
    const navigate = useNavigate()
    
    return (
        <div className="personalDetails">
            <div>
                 <GuestHome />
            </div>
            <div style={styles.table}>
            <table className="table table-striped">
                <td></td>
                
                
                <tbody>
                    
                    <tr style={styles.row}>
                        <th scope="row">1</th>
                        <td border='1px solid black' align='left'>Registration No </td> 
                        <td border='1px solid black' align='left'>{registrationNo}</td>
                    </tr>

                    <tr style={styles.row}>
                        <th scope="row">2</th>
                        <td border='1px solid black' align='left'>First Name </td> 
                        <td border='1px solid black' align='left'>{firstName}</td>
                    </tr>
                    <tr style={styles.row}>
                        <th scope="row">3</th>
                        <td align='left'>Last Name </td> 
                        <td align='left'>{lastName}</td>
                    </tr> 
                    <tr style={styles.row}>
                        <th scope="row">4</th>
                        <td align='left'>Application Status </td>
                        <td align='left'>{applicationStatus}</td>
                    </tr>
                    
                </tbody>
            </table>
                
            </div>
            
            {/* <div className='text-right'>
                            <button onClick={() => {
                                navigate('/edit-details',{ state : { studentDetails : studentDetails } })
                            }} 
                            className="btn btn-secondary btn-sm" type="button"
                            >
                                Edit
                            </button>
                            
                            
                </div> */}

        </div>
    )
}

export default GuestPersonalDetails; 