package com.sunbeam.dto;

import org.springframework.stereotype.Component;

import com.sunbeam.entities.Placement;

@Component
public class EntityDTOConverter {

	public CompanyDetailsDTO toPlacementDto(Placement companyDetails) {
		CompanyDetailsDTO companyDetailsDto = new CompanyDetailsDTO();
		companyDetailsDto.setCompanyId(companyDetails.getCompanyId());
		companyDetailsDto.setCompanyName(companyDetails.getCompanyName());
		companyDetailsDto.setCompanyType(companyDetails.getCompanyType());
		companyDetailsDto.setVacancies(companyDetails.getVacancies());
		companyDetailsDto.setCompanyCtc(companyDetails.getCompanyCtc());
		return companyDetailsDto;
	}

	public CompanyDetailsDTO toListOfPlacementDTO(Placement companyDetails) {
		if (companyDetails == null)
			return null;
		CompanyDetailsDTO companyDetailsDto = new CompanyDetailsDTO();
		companyDetailsDto.setCompanyId(companyDetails.getCompanyId());
		companyDetailsDto.setCompanyName(companyDetails.getCompanyName());
		companyDetailsDto.setCompanyType(companyDetails.getCompanyType());
		companyDetailsDto.setVacancies(companyDetails.getVacancies());
		companyDetailsDto.setCompanyCtc(companyDetails.getCompanyCtc());
		return companyDetailsDto;
	}

	public Placement toCompanyEntity( CompanyDTO addCompanyDto) {
		Placement companyDetails = new Placement();
		companyDetails.setCompanyId(addCompanyDto.getCompanyId());
		companyDetails.setCompanyName(addCompanyDto.getCompanyName());
		companyDetails.setCompanyType(addCompanyDto.getCompanyType());
		companyDetails.setVacancies(addCompanyDto.getVacancies());
		companyDetails.setCompanyCtc(addCompanyDto.getCompanyCtc());
		companyDetails.setDegreeScore(addCompanyDto.getDegreeScore());
		companyDetails.setDiplomaScore(addCompanyDto.getDiplomaScore());
		companyDetails.setHscScore(addCompanyDto.getHscScore());
		companyDetails.setSscScore(addCompanyDto.getSscScore());
		return companyDetails;
	}

	public CompanyEligibilityDTO toCompanyEligibiityCriteriaDto(Placement companyEligibiityCriteria) {
		CompanyEligibilityDTO companyEligibiityCriteriaDto = new CompanyEligibilityDTO();
		companyEligibiityCriteriaDto.setCompanyName(companyEligibiityCriteria.getCompanyName());
		companyEligibiityCriteriaDto.setDegreeScore(companyEligibiityCriteria.getDegreeScore());
		companyEligibiityCriteriaDto.setDiplomaScore(companyEligibiityCriteria.getDiplomaScore());
		companyEligibiityCriteriaDto.setHscScore(companyEligibiityCriteria.getHscScore());
		companyEligibiityCriteriaDto.setSscScore(companyEligibiityCriteria.getSscScore());
		return companyEligibiityCriteriaDto;
	}

	public CompanyListDTO toCompanyListDto(Placement company) {
		CompanyListDTO companyDto = new CompanyListDTO();
		companyDto.setCompanyId(company.getCompanyId());
		companyDto.setCompanyName(company.getCompanyName());
		return companyDto;
	}

	public Placement toCompanyDetailsEntity(CompanyDetailsDTO companyDetailsDto, Placement companyDetails) {
		Placement newCompanyDetails = new Placement();
		newCompanyDetails.setCompanyId(companyDetailsDto.getCompanyId());
		
		if(companyDetailsDto.getCompanyName()==null)
			newCompanyDetails.setCompanyName(companyDetails.getCompanyName());
		else
		newCompanyDetails.setCompanyName(companyDetailsDto.getCompanyName());
		
		if(companyDetailsDto.getCompanyType()==null)
			newCompanyDetails.setCompanyType(companyDetails.getCompanyType());
		else
		newCompanyDetails.setCompanyType(companyDetailsDto.getCompanyType());
		
		if(companyDetailsDto.getVacancies()==0.0)
			newCompanyDetails.setVacancies(companyDetails.getVacancies());
		else
		newCompanyDetails.setVacancies(companyDetailsDto.getVacancies());
		
		if(companyDetailsDto.getCompanyCtc()==0.0)
			newCompanyDetails.setCompanyCtc(companyDetails.getCompanyCtc());
		else
		newCompanyDetails.setCompanyCtc(companyDetailsDto.getCompanyCtc());
		
		newCompanyDetails.setDegreeScore(companyDetails.getDegreeScore());
		newCompanyDetails.setSscScore(companyDetails.getSscScore());
		newCompanyDetails.setDiplomaScore(companyDetails.getDiplomaScore());
		newCompanyDetails.setHscScore(companyDetails.getHscScore());
		return newCompanyDetails;
	}

	public Placement toCompanyEligibilityEntity(CompanyEligibilityDTO companyEligibilityDto, Placement companyEligibilityCriteria) {
		Placement newCompanyEligibilityCriteria = new Placement();
		newCompanyEligibilityCriteria.setCompanyId(companyEligibilityCriteria.getCompanyId());
		newCompanyEligibilityCriteria.setCompanyName(companyEligibilityCriteria.getCompanyName());
		newCompanyEligibilityCriteria.setCompanyType(companyEligibilityCriteria.getCompanyType());
		newCompanyEligibilityCriteria.setVacancies(companyEligibilityCriteria.getVacancies());
		newCompanyEligibilityCriteria.setCompanyCtc(companyEligibilityCriteria.getCompanyCtc());
		
		if(companyEligibilityDto.getDegreeScore()==0.0)
			newCompanyEligibilityCriteria.setDegreeScore(companyEligibilityCriteria.getDegreeScore());
		else
		newCompanyEligibilityCriteria.setDegreeScore(companyEligibilityDto.getDegreeScore());
		
		if(companyEligibilityDto.getSscScore()==0.0)
			newCompanyEligibilityCriteria.setSscScore(companyEligibilityCriteria.getSscScore());
		else
		newCompanyEligibilityCriteria.setSscScore(companyEligibilityDto.getSscScore());
		
		if(companyEligibilityDto.getDiplomaScore()==0.0)
			newCompanyEligibilityCriteria.setDiplomaScore(companyEligibilityCriteria.getDiplomaScore());
		else
		newCompanyEligibilityCriteria.setDiplomaScore(companyEligibilityDto.getDiplomaScore());
		
		if(companyEligibilityDto.getHscScore()==0.0)
			newCompanyEligibilityCriteria.setHscScore(companyEligibilityCriteria.getHscScore());
		else
		newCompanyEligibilityCriteria.setHscScore(companyEligibilityDto.getHscScore());
		
		return newCompanyEligibilityCriteria;
	}
	
	
}
