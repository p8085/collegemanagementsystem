package com.sunbeam.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "subject")
public class Subject {
	@Id
	@Column(name = "subject_id")
	private int subjectId;
	@Column(name = "subject_name")
	private String subjectName;
	@ManyToOne
	private Department department_id;
	@ManyToMany(mappedBy = "subjects")
	private List<StaffMember> staff;

	public Subject() {
		super();
	}

	public Subject(int subjectId, String subjectName, Department department_id, List<StaffMember> staff) {
		super();
		this.subjectId = subjectId;
		this.subjectName = subjectName;
		this.department_id = department_id;
		this.staff = staff;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public Department getDepartment_id() {
		return department_id;
	}

	public void setDepartment_id(Department department_id) {
		this.department_id = department_id;
	}

	public List<StaffMember> getStaff() {
		return staff;
	}

	public void setStaff(List<StaffMember> staff) {
		this.staff = staff;
	}

	@Override
	public String toString() {
		return "Subject [subjectId=" + subjectId + ", subjectName=" + subjectName + ", department_id=" + department_id
				+ ", staff=" + staff + "]";
	}

}
