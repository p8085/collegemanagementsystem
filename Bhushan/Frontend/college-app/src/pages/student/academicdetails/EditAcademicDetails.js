import { useLocation } from "react-router"
import axios from 'axios'
import { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { URL } from '../../../config'
import StudentHome from '../../../components/home/studenthome'

const EditAcademicDetails=()=>{
    const { state } = useLocation()
    const { academicRecords } = state
    console.log(academicRecords)
    const navigate = useNavigate()
    const { enrollmentNo } = sessionStorage
    const [diplomaScore, setDiplomaScore] = useState(`${academicRecords.diplomaScore}`)
    const [hscScore, setHscScore] = useState(`${academicRecords.hscScore}`)
    const [sscScore, setSscScore] = useState(`${academicRecords.sscScore}`)
    
    const editRecord=()=>{
        const body = {
            sscScore,
            diplomaScore,
           hscScore
        }

        const url = `${URL}/student/updateAcademicDetails/${enrollmentNo}`
    console.log({url})
    axios.patch(url, body).then((response) => {
      // get the data from the response
      const result = response.data
      console.log(result)
      if (result['status'] == 'success') {
        toast.success('Successfully edited record')

        
        navigate('/academicDetails')
      } else {
        toast.error(result['error'])
      }
    })
    }
    
    
    
    
    return(
        <div>
        <div>
            <div >
                <StudentHome />
            </div>
        <h1 className="title">EditStudent</h1>

  <div className="row">
    <div className="col"></div>
    <div className="col">
   
      <div className="form">
          
        <div className="mb-3">
          <label htmlFor="" className="label-control">
            Diploma Score
          </label>
          <input
            onChange={(e) => {
              setDiplomaScore(e.target.value)
            }}
            type="text"
            className="form-control"  
          />
        </div>
        <div className="mb-3">
          <label htmlFor="" className="label-control">
            HSC Score
          </label>
          <input
            onChange={(e) => {
              setHscScore(e.target.value)
            }}
            type="text"
            className="form-control" 
          />
        </div>

        <div className="mb-3">
          <label htmlFor="" className="label-control">
            SSC Score
          </label>
          <input
            onChange={(e) => {
              setSscScore(e.target.value)
            }}
            type="text"
            className="form-control" 
          />
        </div>

        

        <div className="mb-3">
          
          <button onClick={editRecord} className="btn btn-secondary float-start">
            Save
          </button>
            <Link to="/academicDetails" className="btn btn-secondary float-end">
                Cancel
            </Link>
                            
        </div>
      </div>
 
    </div>
    <div className="col"></div>
  </div>
</div>
        
    </div>
    )
}

export default EditAcademicDetails