package com.sunbeam.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sunbeam.entities.Guest;

@Repository
public interface GuestDao extends JpaRepository<Guest, Integer>{
	Guest findByEmail(String email);
}
