package com.sunbeam.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "admission_form")
public class AdmissionForm {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "form_no")
	private int formNo;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "middle_name")
	private String middleName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "ssc_score")
	private double sscScore;
	@Column(name = "hsc_score")
	private double hscScore;
	@Column(name = "diploma_score")
	private double diplomaScore;
	@Column(name = "cet_score")
	private double cetScore;
	@Column(name = "address_line1")
	private String addressLine1;
	@Column(name = "address_line2")
	private String addressLine2;
	@Column(name = "zip_code")
	private int zipCode;
	@OneToOne
	private Guest guestId;

	public AdmissionForm() {
		super();
	}

	public AdmissionForm(int formNo, String firstName, String middleName, String lastName, double sscScore,
			double hscScore, double diplomaScore, double cetScore, String addressLine1, String addressLine2,
			int zipCode, Guest guestId) {
		super();
		this.formNo = formNo;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.sscScore = sscScore;
		this.hscScore = hscScore;
		this.diplomaScore = diplomaScore;
		this.cetScore = cetScore;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.zipCode = zipCode;
		this.guestId = guestId;
	}

	public int getFormNo() {
		return formNo;
	}

	public void setFormNo(int formNo) {
		this.formNo = formNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getSscScore() {
		return sscScore;
	}

	public void setSscScore(double sscScore) {
		this.sscScore = sscScore;
	}

	public double getHscScore() {
		return hscScore;
	}

	public void setHscScore(double hscScore) {
		this.hscScore = hscScore;
	}

	public double getDiplomaScore() {
		return diplomaScore;
	}

	public void setDiplomaScore(double diplomaScore) {
		this.diplomaScore = diplomaScore;
	}

	public double getCetScore() {
		return cetScore;
	}

	public void setCetScore(double cetScore) {
		this.cetScore = cetScore;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public Guest getGuestId() {
		return guestId;
	}

	public void setGuestId(Guest guestId) {
		this.guestId = guestId;
	}

	@Override
	public String toString() {
		return "AdmissionForm [formNo=" + formNo + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", sscScore=" + sscScore + ", hscScore=" + hscScore + ", diplomaScore="
				+ diplomaScore + ", cetScore=" + cetScore + ", addressLine1=" + addressLine1 + ", addressLine2="
				+ addressLine2 + ", zipCode=" + zipCode + ", guestId=" + guestId + "]";
	}

}