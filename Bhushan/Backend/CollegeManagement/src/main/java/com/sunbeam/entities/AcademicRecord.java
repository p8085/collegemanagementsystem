package com.sunbeam.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "academic_record")
public class AcademicRecord {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "enrollment_no")
	private int enrollmentNo;
	@Column(name = "ssc_score")
	private double sscScore;
	@Column(name = "hsc_score")
	private double hscScore;
	@Column(name = "diploma_score")
	private double diplomaScore;
	@Column(name = "first_sem")
	private double firstSem;
	@Column(name = "second_sem")
	private double secondSem;
	@Column(name = "third_sem")
	private double thirdSem;
	@Column(name = "fourth_sem")
	private double fourthSem;
	@Column(name = "fifth_sem")
	private double fifthSem;
	@Column(name = "sixth_sem")
	private double sixthSem;
	@Column(name = "seventh_sem")
	private double seventhSem;
	@Column(name = "eighth_sem")
	private double eighthSem;
	private char grade;

	public AcademicRecord() {
		super();
	}

	public AcademicRecord(int enrollmentNo, double sscScore, double hscScore, double diplomaScore, double firstSem,
			double secondSem, double thirdSem, double fourthSem, double fifthSem, double sixthSem, double seventhSem,
			double eighthSem, char grade) {
		super();
		this.enrollmentNo = enrollmentNo;
		this.sscScore = sscScore;
		this.hscScore = hscScore;
		this.diplomaScore = diplomaScore;
		this.firstSem = firstSem;
		this.secondSem = secondSem;
		this.thirdSem = thirdSem;
		this.fourthSem = fourthSem;
		this.fifthSem = fifthSem;
		this.sixthSem = sixthSem;
		this.seventhSem = seventhSem;
		this.eighthSem = eighthSem;
		this.grade = grade;
	}

	public int getEnrollmentNo() {
		return enrollmentNo;
	}

	public void setEnrollmentNo(int enrollmentNo) {
		this.enrollmentNo = enrollmentNo;
	}

	public double getSscScore() {
		return sscScore;
	}

	public void setSscScore(double sscScore) {
		this.sscScore = sscScore;
	}

	public double getHscScore() {
		return hscScore;
	}

	public void setHscScore(double hscScore) {
		this.hscScore = hscScore;
	}

	public double getDiplomaScore() {
		return diplomaScore;
	}

	public void setDiplomaScore(double diplomaScore) {
		this.diplomaScore = diplomaScore;
	}

	public double getFirstSem() {
		return firstSem;
	}

	public void setFirstSem(double firstSem) {
		this.firstSem = firstSem;
	}

	public double getSecondSem() {
		return secondSem;
	}

	public void setSecondSem(double secondSem) {
		this.secondSem = secondSem;
	}

	public double getThirdSem() {
		return thirdSem;
	}

	public void setThirdSem(double thirdSem) {
		this.thirdSem = thirdSem;
	}

	public double getFourthSem() {
		return fourthSem;
	}

	public void setFourthSem(double fourthSem) {
		this.fourthSem = fourthSem;
	}

	public double getFifthSem() {
		return fifthSem;
	}

	public void setFifthSem(double fifthSem) {
		this.fifthSem = fifthSem;
	}

	public double getSixthSem() {
		return sixthSem;
	}

	public void setSixthSem(double sixthSem) {
		this.sixthSem = sixthSem;
	}

	public double getSeventhSem() {
		return seventhSem;
	}

	public void setSeventhSem(double seventhSem) {
		this.seventhSem = seventhSem;
	}

	public double getEighthSem() {
		return eighthSem;
	}

	public void setEighthSem(double eighthSem) {
		this.eighthSem = eighthSem;
	}

	public char getGrade() {
		return grade;
	}

	public void setGrade(char grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "AcademicRecord [enrollmentNo=" + enrollmentNo + ", sscScore=" + sscScore + ", hscScore=" + hscScore
				+ ", diplomaScore=" + diplomaScore + ", firstSem=" + firstSem + ", secondSem=" + secondSem
				+ ", thirdSem=" + thirdSem + ", fourthSem=" + fourthSem + ", fifthSem=" + fifthSem + ", sixthSem="
				+ sixthSem + ", seventhSem=" + seventhSem + ", eighthSem=" + eighthSem + ", grade=" + grade + "]";
	}

}
