package com.sunbeam.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "faculty_paper")
public class FacultyPaper {
	@Id
	@Column(name = "employee_id")
	private int employeeId;
	@Column(name = "paper_topic")
	private String paperTopic;
	@Column(name = "paper_desc")
	private String paperDesc;

	@ManyToOne
	private StaffMember faculty;

	public FacultyPaper() {
		super();
	}

	public FacultyPaper(int employeeId, String paperTopic, String paperDesc) {
		super();
		this.employeeId = employeeId;
		this.paperTopic = paperTopic;
		this.paperDesc = paperDesc;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getPaperTopic() {
		return paperTopic;
	}

	public void setPaperTopic(String paperTopic) {
		this.paperTopic = paperTopic;
	}

	public String getPaperDesc() {
		return paperDesc;
	}

	public void setPaperDesc(String paperDesc) {
		this.paperDesc = paperDesc;
	}

	@Override
	public String toString() {
		return "FacultyPaper [employeeId=" + employeeId + ", paperTopic=" + paperTopic + ", paperDesc=" + paperDesc
				+ "]";
	}

}
