import { useState } from 'react'
import { toast } from 'react-toastify'
import { URL } from '../../config'
import { Link, useNavigate } from 'react-router-dom'
import StudentHome from '../../components/home/studenthome'
import axios from 'axios'
const styles={
    div :{
        
        padding : '10px',
        margin : '10px',
        align : 'center'
    },
    input :{
        margin :'15px',
        width : '70%'
    },
    button : {
        align : 'center'
    }
}
const ChangePassword=()=>
{
    const { enrollmentNo, firstName,lastName } = sessionStorage
    const [ password, setPassword ] = useState('')
    const [ confirmPassword, setConfirmPassword ] = useState('')
    const navigate = useNavigate()
    const savePassword = () => {
        console.log(`password=${password}`)
        console.log(`password=${confirmPassword}`)
        if(password.length==0){
            toast.warning('Please enter password')
        }else if(confirmPassword.length==0)
        {
            toast.warning('Please confirm your password')
        }else if(password!=confirmPassword){
            toast.warning('password does not match')
        }else{
            const body = {
                password
              }
          
          
              const url = `${URL}/student/updatePassword/${enrollmentNo}`
              console.log({url})
              axios.put(url, body).then((response) => {
                // get the data from the response
                const result = response.data
                console.log(result)
                if (result['status'] == 'success') {
                  toast.success('Successfully edited student')
          
                  // navigate to the signin page
                  navigate('/personalDetails')
                } else {
                  toast.error(result['error'])
                }
              })
        }
        
      }
    return (
        <div className="changePassword" style={styles.div}>
            <div><StudentHome /></div>
                <div class="container">
                <div class="row">
                    <div class="col-4">
                    </div>
                    <div class="col-8">
                    <div class="mb-3 row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                <input
            onChange={(e) => {
              setPassword(e.target.value)
            }}
            type="text"
            className="form-control" 
            style={styles.input} 
          />
               </div>
            <div class="mb-3 row">
                <label for="confirmPassword" class="col-sm-2 col-form-label">Confirm Password</label>
                <div class="col-sm-10">
                <input
            onChange={(e) => {
              setConfirmPassword(e.target.value)
            }}
            type="text"
            className="form-control"  
            style={styles.input}
          />
            </div>
            </div>
        <div className="mb-3">
          
          <button onClick={savePassword} className="btn btn-secondary pull-right" style={styles.button}>
            Save
          </button>
            
            {/* <Link to="/personalDetails" className="btn btn-secondary float-end">
                Cancel
            </Link> */}
                            
        </div>
        </div>
                    </div>
                </div>
                </div>
        </div>
    )
}

export default ChangePassword