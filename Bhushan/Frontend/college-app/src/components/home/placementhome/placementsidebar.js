import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as AiOutline from 'react-icons/ai';
import { BsFilterSquare } from "react-icons/bs";

export const placementSidebar = [
    {
      title: 'Add Company',
      path: '/admin/addcompany',
      icon: <AiOutline.AiOutlineUser/>,
      cName: 'nav-text'
    },
    {
      title: ' Search Company ',
      path: '/findcompanyby/id/name/type',
      icon: <BsFilterSquare/>,
      cName: 'nav-text'
    },
    {
        title: 'Company Eligibility Criteria',
        path: '/companyeligibilitycriteria',
        icon: <FaIcons.FaEnvelopeOpenText />,
        cName: 'nav-text'
    },
    {
        title: 'All Companies',
        path: '/getallcompanies',
        icon: <AiOutline.AiOutlineLogout />,
        cName: 'nav-text'
    },
    {
        title: 'Update Company Eligibility Criteria',
        path: '/editcompanyeligibilitycriteria',
        icon: <FaIcons.FaEnvelopeOpenText />,
        cName: 'nav-text'
    },
    {
        title: 'Update Company Details',
        path: '/editcompanydetails',
        icon: <FaIcons.FaEnvelopeOpenText />,
        cName: 'nav-text'
    },
    {
        title: 'Delete Company ',
        path: '/deletecompanye',
        icon: <FaIcons.FaEnvelopeOpenText />,
        cName: 'nav-text'
    },
    {
        title: 'FAQ',
        path: '/faq',
        icon: <IoIcons.IoMdHelpCircle />,
        cName: 'nav-text'
    }
  ];
  