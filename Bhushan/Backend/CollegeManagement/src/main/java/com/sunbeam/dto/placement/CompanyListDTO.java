package com.sunbeam.dto.placement;

import org.springframework.stereotype.Component;

@Component
public class CompanyListDTO {

	private int companyId;
	private String companyName;

	public CompanyListDTO() {
		super();
	}

	public CompanyListDTO(int companyId, String companyName) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Override
	public String toString() {
		return "CompanyListDTO [companyId=" + companyId + ", companyName=" + companyName + "]";
	}

}
