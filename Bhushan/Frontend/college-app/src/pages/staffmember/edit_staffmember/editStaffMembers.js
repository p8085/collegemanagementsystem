import axios from "axios";
import { useState } from "react";
import { useLocation, useNavigate } from "react-router";
import { toast } from "react-toastify";
import { URL } from "../../../config";

const EditStaffMember = () => {
  const { state } = useLocation;
  const { staffDetails } = state;
  const [firstName, setFirstName] = useState("");
  const [middleName, setMiddleName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [dob, setDob] = useState("");
  const [workExperience, setWorkExperience] = useState("");
  const [salary, setSalary] = useState("");
  const [hireDate, setHireDate] = useState("");
  const [departmentId, setDepartmentId] = useState("");
  const [contactNo, setContactNo] = useState("");

  const navigateTo = useNavigate();
  const update = () => {
    const body = {
      firstName,
      middleName,
      lastName,
      email,
      dob,
      workExperience,
      salary,
      hireDate,
      departmentId,
      contactNo,
    };

    const url = `${URL}/faculty/update/${staffDetails.employeeId}`;
    axios.patch(url, body).then((response) => {
      const result = response.data;
      if (result["status"] == "success") {
        toast.success("Employee data Updated SUCCESSFULLY!...");
        navigateTo("/admin/add");
      } else {
        toast.error("Data not UPDATED");
      }
    });
  };
  return (
    <div>
      <div className="container">
        <div className="form">
          <div className="row">
            <div className="col-2">Menus</div>
            <div className="col">
              <div className="row">
                <div className="col"></div>
                <div className="col">
                  <h2>Add New Employee</h2>
                </div>
                <div className="col"></div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setFirstName(e.target.value);
                      }}
                      type="text"
                      className="form-control"
                      placeholder="name@example.com"
                    />
                    <label>First Name</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setMiddleName(e.target.value);
                      }}
                      type="text"
                      className="form-control"
                      placeholder="name@example.com"
                    />
                    <label>Middle Name</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setLastName(e.target.value);
                      }}
                      type="text"
                      className="form-control"
                      placeholder="name@example.com"
                    />
                    <label>Last Name</label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setEmail(e.target.value);
                      }}
                      type="email"
                      className="form-control"
                      placeholder="name@example.com"
                    />
                    <label>Email address</label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setDob(e.target.value);
                      }}
                      type="date"
                      className="form-control"
                      id="floatingDob"
                      placeholder="Password"
                    />
                    <label htmlFor="floatingDob">Date of Birth</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setHireDate(e.target.value);
                      }}
                      type="date"
                      className="form-control"
                      id="floatingHireDate"
                      placeholder="Password"
                    />
                    <label htmlFor="floatingHireDate">Hire Date</label>
                  </div>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col">Contact No.</div>
              </div>
              <div className="row">
                <div className="col">
                  <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon1">
                      +91
                    </span>
                    <input
                      onChange={(e) => {
                        setContactNo(e.target.value);
                      }}
                      type="number"
                      className="form-control"
                      placeholder="9876543210"
                      aria-label="Username"
                      aria-describedby="basic-addon1"
                    />
                  </div>
                </div>
                <div className="col"></div>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setSalary(e.target.value);
                      }}
                      type="number"
                      className="form-control"
                      placeholder="Password"
                    />
                    <label>Salary</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        setWorkExperience(e.target.value);
                      }}
                      type="number"
                      className="form-control"
                      placeholder="Password"
                    />
                    <label>Work Experience</label>
                  </div>
                </div>
                <div className="col">
                  <div className="form-floating mb-3">
                    <input
                      onChange={(e) => {
                        const deptId = e.target.value;
                        console.log(deptId);
                        setDepartmentId(e.target.value);
                      }}
                      type="number"
                      className="form-control"
                      id="floatingDepartmentId"
                      placeholder="name@example.com"
                    />
                    <label>Department ID</label>
                  </div>
                </div>
                <div className="col"></div>
                <div className="col"></div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-grid gap-2 d-md-flex justify-content-md-end">
          <button
            onClick={update}
            className="btn btn-primary me-md-2"
            type="button"
          >
            Upadte
          </button>
          {/* <button onClick={cancel} type="button" className="btn btn-danger">
            Cancel
          </button> */}
        </div>
      </div>
    </div>
  );
};

export default EditStaffMember;
