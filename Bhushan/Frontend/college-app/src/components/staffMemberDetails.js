import axios from "axios";
import { URL } from "../config";
import { toast } from "react-toastify";
import { useLocation, useNavigate } from "react-router";
import { formatDate, formatDateAgo } from "../utils";
import { Link } from "react-router-dom";
import { Accordion, Card } from "react-bootstrap";
import { useEffect, useState } from "react";
import FacultyPaper from "../pages/facultypaper/facultyPaper";

const styles = {
  employeeList: {
    backgroundColor: "beige",
  },
  details: {
    color: "darkgray",
  },
  icons: {
    cursor: "pointer",
    width: "20px",
    height: "20px",
    margin: "5px",
  },
};

const StaffMemberDetails = () => {
  //   const {
  //     employeeId,
  //     firstName,
  //     middleName,
  //     lastName,
  //     email,
  //     dod,
  //     workExperience,
  //     hireDate,
  //     contactNo,
  //     gender,
  //     salary,
  //     facultyPaper,
  //     subjects,
  //   } = props;
  const [employeeData, setEmployeeData] = useState();
  const [facultyPapers, setFacultyPaper] = useState([]);

  const navigateTo = useNavigate();

  const url = `${URL}/admin/allStaff`;

  const loadStaffMemberDetails = async () => {
    const response = await axios.get(url);
    setEmployeeData(response.data.data);
  };

  const displayPapers = (paperList) => {
    if (paperList.length == 0) {
      return <li style={{ color: "red" }}>No Paper Published Yet!...</li>;
    } else {
      return paperList.map((paper) => {
        return (
          <div>
            <table>
              <tr>
                <td>
                  <h6>Topic : </h6>
                </td>
                <td>{paper.paperTopic}</td>
              </tr>
              <tr>
                <td>
                  <h6>Description : </h6>
                </td>
                <td>{paper.paperDesc}</td>
              </tr>
            </table>
          </div>
        );
      });
    }
  };

  const deleteEmployee = (id) => {
    axios.delete(`${URL}/admin/delete/employee/${id}`).then((response) => {
      const result = response.data;
      if (result["status"] == "success") {
        toast.success("Employee Deleted Successfully!...");
        navigateTo("/admin/allStaffMembers");
      } else {
        toast.error("Employee not DELETED!...");
      }
    });
  };

  const getFacultyPapers = async (id) => {
    const response = await axios.get(`${URL}/faculty/paperPublished/${id}`);
    setFacultyPaper(response.data);
  };

  const renderAccordion = (employee, index) => {
    return (
      <div className="row">
        <div className="col-3"> </div>
        <div className="col">
          <Accordion defaultActiveKey={index} flush>
            <Accordion.Item eventKey={employee}>
              <Accordion.Header>
                <div className="col">
                  <h6>
                    {employee.employeeId}. {employee.firstName}{" "}
                    {employee.middleName} {employee.lastName}
                  </h6>
                </div>
                <div className="d-flex justify-content-between">
                  <Link to={`/admin/edit-employee`}>
                    <img
                      onClick={() => {
                        navigateTo("/admin/edit-employee", {
                          state: { employeeDetails: employee },
                        });
                      }}
                      style={styles.icons}
                      src={require("../assets/Edit.png")}
                    />
                  </Link>
                </div>
                <div className="d-flex justify-content-between">
                  <Link to={`/admin/delete/employee/${employee.employeeId}`}>
                    <img
                      onClick={() => {
                        deleteEmployee(employee.employeeId);
                      }}
                      style={styles.icons}
                      src={require("../assets/Delete.png")}
                    />
                  </Link>
                </div>
              </Accordion.Header>
              <Accordion.Body>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item">
                    Gender : {employee.gender}
                  </li>
                  <li className="list-group-item">email : {employee.email}</li>
                  <li className="list-group-item">
                    Date of Birth : {employee.dob}
                  </li>
                  <li className="list-group-item">
                    Hire Date : {employee.hireDate}
                  </li>
                  <li className="list-group-item">
                    Salary : {employee.salary}
                  </li>
                  <li className="list-group-item">
                    Work Experience : {employee.workExperience}
                  </li>
                  <li className="list-group-item">
                    Contact No. : +91 {employee.contactNo}
                  </li>
                  <li className="list-group-item">
                    Department :{" "}
                    <ul>
                      <li>ID : {employee.department.departmentId}</li>
                      <li> Name : {employee.department.departmentName}</li>
                    </ul>
                  </li>
                  <li className="list-group-item">
                    <div
                      class="accordion accordion-flush"
                      id="accordionFlushExample"
                    >
                      <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                          <button
                            class="accordion-button collapsed"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseOne"
                            aria-expanded="false"
                            aria-controls="flush-collapseOne"
                          >
                            # Paper Published : {employee.facultyPapers.length}
                          </button>
                        </h2>
                        <div
                          id="flush-collapseOne"
                          class="accordion-collapse collapse"
                          aria-labelledby="flush-headingOne"
                          data-bs-parent="#accordionFlushExample"
                        >
                          <div class="accordion-body">
                            <li>{displayPapers(employee.facultyPapers)}</li>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </div>
      </div>
    );
  };

  useEffect(() => {
    loadStaffMemberDetails();
  }, []);

  useEffect(() => {
    getFacultyPapers();
  }, []);

  return (
    <div>
      <div>{employeeData && employeeData.map(renderAccordion)}</div>
      {/* {console.log(employeeData)} */}
    </div>
  );
};

export default StaffMemberDetails;
