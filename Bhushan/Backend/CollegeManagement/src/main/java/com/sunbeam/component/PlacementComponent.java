package com.sunbeam.component;

import org.springframework.stereotype.Component;

import com.sunbeam.dto.placement.CompanyDetailsDTO;
import com.sunbeam.dto.placement.CompanyEligibilityDTO;

@Component
public class PlacementComponent {

	public boolean isJASONObjectIsEmpty(Object object) {

		if (object instanceof CompanyDetailsDTO) {
			if (((CompanyDetailsDTO) object).getCompanyName() == null
					&& ((CompanyDetailsDTO) object).getCompanyType() == null
					&& ((CompanyDetailsDTO) object).getCompanyCtc() == 0.0
					&& ((CompanyDetailsDTO) object).getVacancies() == 0.0)
				return true;
			return false;
		}

		if (object instanceof CompanyEligibilityDTO) {
			if (((CompanyEligibilityDTO) object).getDegreeScore() == 0.0
					&& ((CompanyEligibilityDTO) object).getDiplomaScore() == 0.0
					&& ((CompanyEligibilityDTO) object).getHscScore() == 0.0
					&& ((CompanyEligibilityDTO) object).getSscScore() == 0.0)
				return true;
			return false;
		}
		return false;
	}
}
