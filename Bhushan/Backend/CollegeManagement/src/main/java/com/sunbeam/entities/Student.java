package com.sunbeam.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "student")
public class Student {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "enrollment_no")
	private int enrollmentNo;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "middle_name")
	private String middleName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "email", unique = true)
	private String email;
	@Column(name = "password", length = 1024)
	private String password;
	@Temporal(TemporalType.DATE)
	@Column(name = "dob")
	private Date dob;
	@Column(name = "admission_type")
	private String admissionType;
	@Column(name = "current_sem")
	private String currentSem;
	@Column(name = "contact_no")
	private String contactNo;
	@Column(name = "gender")
	private String gender;
	@Column(name = "roll_no")
	private String rollNo;
	@Column(name = "address_line1")
	private String addressLine1;
	@Column(name = "address_line2")
	private String addressLine2;
	@Column(name = "zip_id")
	private int zipId;
	@Column(name = "lib_fine")
	private double libraryFine;
	private String imageUrl;
	@ManyToOne
	@JoinColumn(name = "department_id")
	private Department department_id;

	public Student() {
		super();
	}

	public Student(int enrollmentNo, String firstName, String middleName, String lastName, String email,
			String password, Date dob, String admissionType, String currentSem, String contactNo, String gender,
			String rollNo, String addressLine1, String addressLine2, int zipId, double libraryFine, String imageUrl,
			Department department_id) {
		super();
		this.enrollmentNo = enrollmentNo;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.dob = dob;
		this.admissionType = admissionType;
		this.currentSem = currentSem;
		this.contactNo = contactNo;
		this.gender = gender;
		this.rollNo = rollNo;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.zipId = zipId;
		this.libraryFine = libraryFine;
		this.imageUrl = imageUrl;
		this.department_id = department_id;
	}

	public int getEnrollmentNo() {
		return enrollmentNo;
	}

	public void setEnrollmentNo(int enrollmentNo) {
		this.enrollmentNo = enrollmentNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getAdmissionType() {
		return admissionType;
	}

	public void setAdmissionType(String admissionType) {
		this.admissionType = admissionType;
	}

	public String getCurrentSem() {
		return currentSem;
	}

	public void setCurrentSem(String currentSem) {
		this.currentSem = currentSem;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getRollNo() {
		return rollNo;
	}

	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public int getZipId() {
		return zipId;
	}

	public void setZipId(int zipId) {
		this.zipId = zipId;
	}

	public double getLibraryFine() {
		return libraryFine;
	}

	public void setLibraryFine(double libraryFine) {
		this.libraryFine = libraryFine;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Department getDepartment_id() {
		return department_id;
	}

	public void setDepartment_id(Department department_id) {
		this.department_id = department_id;
	}

	@Override
	public String toString() {
		return "Student [enrollmentNo=" + enrollmentNo + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", email=" + email + ", password=" + password + ", dob=" + dob
				+ ", admissionType=" + admissionType + ", currentSem=" + currentSem + ", contactNo=" + contactNo
				+ ", gender=" + gender + ", rollNo=" + rollNo + ", addressLine1=" + addressLine1 + ", addressLine2="
				+ addressLine2 + ", zipId=" + zipId + ", libraryFine=" + libraryFine + ", imageUrl=" + imageUrl
				+ ", department_id=" + department_id + "]";
	}

}
