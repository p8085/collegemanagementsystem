package com.sunbeam.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sunbeam.entities.Placement;

@Repository
public interface PlacementDao extends JpaRepository<Placement, Integer> {
	Placement findByCompanyId(int companyId);

	Placement findByCompanyName(String companyName);

	List<Placement> findByCompanyType(String companyType);
}
