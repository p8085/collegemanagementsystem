package com.sunbeam.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.AdmissionForm;

public interface AdmissionFormDao extends JpaRepository<AdmissionForm, Integer>{

}
