import { useState } from 'react'
import { Link } from 'react-router-dom'
import './index.css'
import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import { URL } from '../../../config'
import StudentHome from '../../../components/home/studenthome'

const styles ={
  div:{
    backgroundColor : 'black',
    height : '80px',
    alignItem:'center'
  },
  form : {
    padding : '10px',
    marginTop : '10px'
  },
  header:{
    color:'white',
    display:'flex',
    //marginTop:'10px'
    alignItems: 'center',
    justifyContent: 'center'
  },
  sidebar:{
    width :'15%',
    backgroundColor:'black',
    position: 'fixed',
    height: '100%',
    display: 'block',
  }

}
const StudentSignin = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const navigate = useNavigate()

  const signinUser = () => {
    if (email.length === 0) {
      
      toast.warning('please enter email')
    } else if (password.length === 0) {
      console.log(password)
      toast.warning('please enter password')
    } else {
      const body = {
        email,
        password,
      }

      // url to make signin api call
      const url = `${URL}/student/signin`
      console.log({url})
      // make api call using axios
      axios.post(url, body).then((response) => {
        
        // get the server result
        const result = response.data
        console.log(result)
        if (result['status'] === 'success') {
          toast.success('Welcome to the application')

          // get the data sent by server
          const { enrollmentNo, firstName, lastName } = result['data']

          // persist the logged in user's information for future use
          sessionStorage['enrollmentNo'] = enrollmentNo
          sessionStorage['firstName'] = firstName
          sessionStorage['lastName'] = lastName
          sessionStorage['loginStatus'] = 1
          console.log({lastName})
          // navigate to home component
        navigate('/studenthome')
        } else {
          toast.error('Invalid user name or password')
        }
      })
    }
  }

  return (
    <div>
      
      <div style={styles.div}>
        <h3 style={styles.header}>Student Login</h3>
      </div>
      
      <div className="row">
        <div className="col"></div>
        <div className="col">
          <div className="form" >
            <div className="mb-3">
              <label htmlFor="" className="label-control" style={styles.form}>
                Email address
              </label>
              <input
                onChange={(e) => {
                  setEmail(e.target.value)
                }}
                type="email"
                className="form-control"
                style={styles.form}
              />
            </div>

            <div className="mb-3" >
              <label htmlFor="" className="label-control">
                Password
              </label>
              <input
                onChange={(e) => {
                  setPassword(e.target.value)
                }}
                type="password"
                className="form-control"
                style={styles.form}
                
              />
            </div>

            <div className="mb-3">
             
              <button onClick={signinUser} className="btn btn-primary" style={styles.form}>
                Signin
              </button>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  )
}

export default StudentSignin
