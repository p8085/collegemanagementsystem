import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as AiOutline from 'react-icons/ai';
import * as  BsFill  from "react-icons/bs";
export const studentSidebar = [
    {
      title: 'PersonalDetaills',
      path: '/personalDetails',
      icon: <AiIcons.AiFillHome />,
      cName: 'nav-text'
    },
    {
      title: 'AcademicDetails',
      path: '/academicDetails',
      icon: <IoIcons.IoMdPeople />,
      cName: 'nav-text'
    },
    {
        title: 'ChangePassword',
        path: '/changePassword',
        icon: <FaIcons.FaEnvelopeOpenText />,
        cName: 'nav-text'
    },
    {
        title: 'Logout',
        path: '/logout',
        icon: <AiOutline.AiOutlineLogout />,
        cName: 'nav-text'
    },
    {
        title: 'FAQ',
        path: '/faq',
        icon: <IoIcons.IoMdHelpCircle />,
        cName: 'nav-text'
    },
    {
      title: 'GetAllStudents',
      path: '/getAllStudents',
      icon: <BsFill.BsFillPersonBadgeFill />,
      cName: 'nav-text'
  },

    
  ];
  