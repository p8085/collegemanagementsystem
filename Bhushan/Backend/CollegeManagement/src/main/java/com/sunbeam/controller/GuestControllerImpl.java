package com.sunbeam.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.component.Response;
import com.sunbeam.dto.guest.Credentials;
import com.sunbeam.dto.guest.GuestSignupDTO;
import com.sunbeam.service.GuestServiceImpl;




@RestController
public class GuestControllerImpl {

	@Autowired
	private GuestServiceImpl guestService;
	
	@PostMapping("/guest/signup")
	public ResponseEntity<?> signUp(@RequestBody GuestSignupDTO guestSignupDto) {
		GuestSignupDTO guest = guestService.saveGuest(guestSignupDto);
		return Response.success(guest);
	}
	
	@PostMapping("/guest/signin")
	public ResponseEntity<?> signIn(@Valid @RequestBody Credentials cred) {
		GuestSignupDTO guestSignupDto = guestService.findUserByEmailAndPassword(cred);
		if(guestSignupDto == null)
			return Response.error("guest not found");
		return Response.success(guestSignupDto);
	}
	
	
}
