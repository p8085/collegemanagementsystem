package com.sunbeam.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.List;

@Entity
@Table(name = "department")
public class Department {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "department_id")
	private int departmentId;
	@Column(name = "department_name")
	private String departmentName;
	@Column(name = "hod_name")
	private String hodName;
	@Column(name = "first_year_student")
	private int firstYearStudent;
	@Column(name = "second_year_student")
	private int secondYearStudent;
	@Column(name = "third_year_student")
	private int thirdYearStudent;
	@Column(name = "fourth_year_student")
	private int fourthYearStudent;
	@OneToMany(mappedBy = "department_id")
	private List<StaffMember> staff;
	@OneToMany(mappedBy = "department_id")
	private List<Subject> subjects;

	public Department() {
		super();
	}

	public Department(int departmentId, String departmentName, String hodName, int firstYearStudent,
			int secondYearStudent, int thirdYearStudent, int fourthYearStudent, List<StaffMember> staff,
			List<Subject> subjects) {
		super();
		this.departmentId = departmentId;
		this.departmentName = departmentName;
		this.hodName = hodName;
		this.firstYearStudent = firstYearStudent;
		this.secondYearStudent = secondYearStudent;
		this.thirdYearStudent = thirdYearStudent;
		this.fourthYearStudent = fourthYearStudent;
		this.staff = staff;
		this.subjects = subjects;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getHodName() {
		return hodName;
	}

	public void setHodName(String hodName) {
		this.hodName = hodName;
	}

	public int getFirstYearStudent() {
		return firstYearStudent;
	}

	public void setFirstYearStudent(int firstYearStudent) {
		this.firstYearStudent = firstYearStudent;
	}

	public int getSecondYearStudent() {
		return secondYearStudent;
	}

	public void setSecondYearStudent(int secondYearStudent) {
		this.secondYearStudent = secondYearStudent;
	}

	public int getThirdYearStudent() {
		return thirdYearStudent;
	}

	public void setThirdYearStudent(int thirdYearStudent) {
		this.thirdYearStudent = thirdYearStudent;
	}

	public int getFourthYearStudent() {
		return fourthYearStudent;
	}

	public void setFourthYearStudent(int fourthYearStudent) {
		this.fourthYearStudent = fourthYearStudent;
	}

	public List<StaffMember> getStaff() {
		return staff;
	}

	public void setStaff(List<StaffMember> staff) {
		this.staff = staff;
	}

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	@Override
	public String toString() {
		return "Department [departmentId=" + departmentId + ", departmentName=" + departmentName + ", hodName="
				+ hodName + ", firstYearStudent=" + firstYearStudent + ", secondYearStudent=" + secondYearStudent
				+ ", thirdYearStudent=" + thirdYearStudent + ", fourthYearStudent=" + fourthYearStudent + ", staff="
				+ staff + ", subjects=" + subjects + "]";
	}

}