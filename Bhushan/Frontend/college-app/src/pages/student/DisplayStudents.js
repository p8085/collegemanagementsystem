const DisplayStudents = (props) => {
    const { students } = props
    return (
        <div>
            
            <table class="table">
                
                <tbody>
                    <tr class="table-active">
                        <td>{students.enrollmentNo}</td>
                        <td>{students.firstName}</td>
                        <td>{students.lastName}</td>
                        <td>{students.email}</td>
                        <td >Delete</td>
                        
                    </tr>
                    
                    
                </tbody>
            </table>
          
        </div>
    )
}

export default DisplayStudents