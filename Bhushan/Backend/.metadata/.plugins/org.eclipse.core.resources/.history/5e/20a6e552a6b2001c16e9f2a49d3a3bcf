package com.sunbeam.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dao.PlacementDao;
import com.sunbeam.dto.CompanyDTO;
import com.sunbeam.dto.CompanyDetailsDTO;
import com.sunbeam.dto.CompanyEligibilityDTO;
import com.sunbeam.dto.CompanyListDTO;
import com.sunbeam.dto.EntityDTOConverter;
import com.sunbeam.dto.Response;
import com.sunbeam.entities.Placement;
import com.sunbeam.service.PlacementServiceImpl;

@RestController
public class PlacementControllerImpl {

	@Autowired
	private PlacementServiceImpl placementService;
	
	@Autowired
	private EntityDTOConverter converter;
	
	@Autowired
	private PlacementDao placementDao;

	@GetMapping("/companydetails/{id}")
	public ResponseEntity<?> findByCompanyId(@PathVariable("id") int companyId) {
		CompanyDetailsDTO companyDetails = placementService.findByCompanyId(companyId);
		if (companyDetails == null)
			return Response.error("Company Not Found");
		return Response.success(companyDetails);
	}

	@GetMapping("/companydetails/byname")
	public ResponseEntity<?> findByCompanyName(@RequestBody Placement placement) {
		CompanyDetailsDTO companyDetails = placementService.findByCompanyName(placement);
		if (companyDetails == null)
			return Response.error("Company Not Found");
		return Response.success(companyDetails);
	}

	@GetMapping("/companydetails/bytype")
	public ResponseEntity<?> findByCompanyType(@RequestBody Placement placement) {
		List<CompanyDetailsDTO> companyDetails = placementService.findByCompanyType(placement);
		if (companyDetails == null)
			return Response.error("Company Not Found");
		return Response.success(companyDetails);
	}

	@PostMapping("/addcompany")
	public ResponseEntity<?> addCompany(@RequestBody CompanyDTO addCompanyDTO) {
		Map<String, Object> company = placementService.addCompany(addCompanyDTO);
		if (company == null)
			return Response.error("Company Can Not Be Added");
		return Response.success(company);
	}

	@GetMapping("/companyeligibilitycriteria")
	public ResponseEntity<?> showCompanyEligibiltyCriteria(@RequestBody Placement placement) {
		CompanyEligibilityDTO companyEligibilityDto = placementService.showCompanyEligibiltyCriteria(placement);
		if (companyEligibilityDto == null)
			return Response.error("Company Not Found");
		return Response.success(companyEligibilityDto);
	}

	@GetMapping("/allcompanylist")
	public ResponseEntity<?> findAllCompanies() {
		List<CompanyListDTO> companyList = placementService.findAllCompanies();
		if (companyList == null)
			return Response.error("Company List Not Availabe Right Now !!! ");
		return Response.success(companyList);
	}

	@PutMapping("/companydetails/{id}")
	public ResponseEntity<?> editCompanyDetails(@PathVariable("id") int companyId,
			@RequestBody CompanyDetailsDTO companyDetailsDto) {
		//Placement newCompnayDetails = placementService.editCompanyDetails(companyId, companyDetailsDto);
		Placement companyDetails = placementDao.findByCompanyId(companyId);
		if(companyDetails != null) {
		companyDetails.setCompanyId(companyDetailsDto.getCompanyId());
		if(companyDetailsDto.getCompanyName() != null) {
		companyDetails.setCompanyName(companyDetailsDto.getCompanyName());
		}
		companyDetails.setCompanyType(companyDetailsDto.getCompanyType());
		companyDetails.setVacancies(companyDetailsDto.getVacancies());
		companyDetails.setCompanyCtc(companyDetailsDto.getCompanyCtc());
		companyDetails = placementDao.save(companyDetails);
		return Response.success(companyDetails);
		}
		return Response.error("Company Not Found");
	}

//	@PutMapping("/editprofile/{userId}")
//	public ResponseEntity<?> editprofile(@PathVariable("userId") int userId, @RequestBody UserDTO userDto) {
//		UserDTO user = s.findUserById(userId);
//
//		user.setAddress(userDto.getAddress());
//		user.setEmail(userDto.getEmail());
//		user.setContact(userDto.getContact());
//
//		UserDTO result = s.saveUser(user);
//		return Response.success(result);
//	}
}
