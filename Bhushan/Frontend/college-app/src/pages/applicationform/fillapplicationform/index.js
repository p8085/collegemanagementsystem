import axios from "axios";
import { useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { URL } from "../../../config";
import { useLocation } from "react-router"
import GuestHome from '../../../components/home/guesthome'
import './index.css'

const FillApplicationForm = () => {
    const { state } = useLocation()
    const [firstName, setFirstName] = useState("");
    const [middleName, setMiddletName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [sscScore, setSscScore] = useState("");
    const [hscScore, setHscScore] = useState("");
    const [diplomaScore, setDiplomaScore] = useState("");
    const [cetScore, setCetScore] = useState("");
    const [dob, setDob] = useState("");
    const [admissionType, setAdmissionType] = useState("");
    const [contactNo, setContactNo] = useState("");
    const [gender, setGender] = useState("");
    const [adhaarNo, setAdhaarNo] = useState("");
    const [addressLine1, setAddressLine1] = useState("");
    const [addressLine2, setAddressLine2] = useState("");
    const [zipCode, setZipCode] = useState("");
    const [branchName, setBranchName] = useState("");
    const [year, setYear] = useState("");
    const {registrationNo , applicationStatus }= sessionStorage 


    const navigateTo = useNavigate();

    const save = () => {
        if (firstName.length === 0) {
            toast.warning("First name field can not be empty");
        } else if (middleName.length === 0) {
            toast.warning("Middle name field can not be empty");
        } else if (lastName.length === 0) {
            toast.warning("Last name field can not be empty");
        } else if (email.length === 0) {
            toast.warning("email field can not be empty");
        } else if (sscScore.length === 0) {
            toast.warning("SSC score field can not be empty");
        } else if (hscScore.length === 0) {
            toast.warning("HSC score field can not be empty");
        } else if (diplomaScore.length === 0) {
            toast.warning("Diploma score field can not be empty");
        } else if (cetScore.length === 0) {
            toast.warning("CET score field can not be empty");
        } else if (dob.length === 0) {
            toast.warning("DOB field can not be empty");
        } else if (admissionType.length === 0) {
            toast.warning("Admission Type field can not be empty");
        } else if (contactNo.length === 0) {
            toast.warning("Contact No field can not be empty");
        } else if (gender.length === 0) {
            toast.warning("Gender field can not be empty");
        } else if (adhaarNo.length === 0) {
            toast.warning("Adhaar No field can not be empty");
        } else if (addressLine1.length === 0) {
            toast.warning("Address Line 1 field can not be empty");
        } else if (addressLine2.length === 0) {
            toast.warning("Adress Line 2 field can not be empty");
        } else if (zipCode.length === 0) {
            toast.warning("zip code field can not be empty");
        } else if (branchName.length === 0) {
            toast.warning("brach name field can not be empty");
        } else if (year.length === 0) {
            toast.warning("Year field can not be empty");
        } else {
            const body = {
                firstName,
                middleName,
                lastName,
                email,
                sscScore,
                hscScore,
                diplomaScore,
                cetScore,
                dob,
                admissionType,
                contactNo,
                gender,
                adhaarNo,
                addressLine1,
                addressLine2,
                zipCode,
                branchName,
                year,
            };

            const url = `${URL}/fillapplicationform/${registrationNo}`

            axios.post(url, body).then((response) => {
                const result = response.data;
                if (result["status"] == "success") {
                    toast.success("Application Submitted Successfully");
                    navigateTo("/guesthome");
                } else {
                    toast.error(result["error"]);
                }
            });
        }
    };

    const cancel = () => {
        navigateTo("/guesthome");
    };

if(applicationStatus === "NOT_APPLIED") {
    return (
        <div>
        <div>
            <GuestHome />
        </div>
            <div className="container">
                <div className="form">
                    <div className="row">
                        <div className="col-2">Menus</div>
                        <div className="col">
                            <div className="row">
                                <div className="col"></div>
                                <div className="col">
                                    <h2>Application Form</h2>
                                </div>
                                <div className="col"></div>
                            </div>

                            <div className="row">
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setFirstName(e.target.value);
                                            }}
                                            type="text"
                                            className="form-control"
                                            placeholder="name@example.com"
                                        />
                                        <label>First Name</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setMiddletName(e.target.value);
                                            }}
                                            type="text"
                                            className="form-control"
                                            placeholder="name@example.com"
                                        />
                                        <label>Middle Name</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setLastName(e.target.value);
                                            }}
                                            type="text"
                                            className="form-control"
                                            placeholder="name@example.com"
                                        />
                                        <label>Last Name</label>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setEmail(e.target.value);
                                            }}
                                            type="email"
                                            className="form-control"
                                            placeholder="name@example.com"
                                        />
                                        <label>Email address</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <select
                                            onChange={(e) => {
                                                const selectedGender = e.target.value;
                                                setGender(selectedGender);
                                            }}
                                            className="form-select"
                                            id="floatingSelectGender"
                                            aria-label="Floating label select example"
                                        >
                                            <option defaultValue="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        <label htmlFor="floatingSelectGender">Gender</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setDob(e.target.value);
                                            }}
                                            type="date"
                                            className="form-control"
                                            id="floatingDob"
                                            placeholder="Password"
                                        />
                                        <label htmlFor="floatingDob">Date of Birth</label>
                                    </div>
                                </div>
                                <div className="col"></div>
                            </div>
                            <div className="row mb-3">
                                <div className="col">Contact No.</div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="input-group mb-3">
                                        <span className="input-group-text" id="basic-addon1">
                                            +91
                                        </span>
                                        <input
                                            onChange={(e) => {
                                                setContactNo(e.target.value);
                                            }}
                                            type="number"
                                            className="form-control"
                                            placeholder="9876543210"
                                            aria-label="Username"
                                            aria-describedby="basic-addon1"
                                        />
                                    </div>
                                </div>
                                <div className="col"></div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setSscScore(e.target.value);
                                            }}
                                            type="number"
                                            className="form-control"
                                            placeholder="ssc marks"
                                        />
                                        <label htmlFor="floatingDob">SSC Score</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setHscScore(e.target.value);
                                            }}
                                            type="number"
                                            className="form-control"
                                            placeholder="hsc marks"
                                        />
                                        <label htmlFor="floatingHireDate">HSC Score</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setDiplomaScore(e.target.value);
                                            }}
                                            type="number"
                                            className="form-control"
                                            placeholder="diploma marks"
                                        />
                                        <label htmlFor="floatingDob">Diploma Score</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setCetScore(e.target.value);
                                            }}
                                            type="number"
                                            className="form-control"
                                            placeholder="CET marks"
                                        />
                                        <label htmlFor="floatingHireDate">CET Score</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setAdhaarNo(e.target.value);
                                            }}
                                            type="number"
                                            className="form-control"
                                            placeholder="adhaar number"
                                        />
                                        <label htmlFor="floatingDob">Adhaar No</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setAdmissionType(e.target.value);
                                            }}
                                            type="text"
                                            className="form-control"
                                            placeholder="Password"
                                        />
                                        <label htmlFor="floatingHireDate">Admission Type</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <select
                                            onChange={(e) => {
                                                const selectedBranchName = e.target.value;
                                                setBranchName(selectedBranchName);
                                            }}
                                            className="form-select"
                                            id="floatingSelectBranchName"
                                            aria-label="Floating label select example"
                                        >
                                            <option defaultValue="">Select</option>
                                            <option value="MECHANICAL">Mechanical</option>
                                            <option value="CIVIL">Civil</option>
                                            <option value="ELECTRICAL">Electrical</option>
                                            <option value="ENTC">Electronics and Telecommunication</option>
                                            <option value="CSE">Computer Science</option>
                                        </select>
                                        <label htmlFor="floatingSelectBranchName">Branch Name</label>
                                    </div>
                                </div>
                                 <div className="col">
                                    <div className="form-floating mb-3">
                                        <select
                                            onChange={(e) => {
                                                const selectedYear = e.target.value;
                                                setYear(selectedYear);
                                            }}
                                            className="form-select"
                                            id="floatingSelectYear"
                                            aria-label="Floating label select example"
                                        >
                                            <option defaultValue="">Select</option>
                                            <option value="FIRST">1st Year</option>
                                            <option value="SECOND">2nd Year</option>
                                            <option value="THIRD">3rd Year</option>
                                            <option value="FOURTH">4th Year</option>
                                        </select>
                                        <label htmlFor="floatingSelectYear">Year</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setAddressLine1(e.target.value);
                                            }}
                                            type="text"
                                            className="form-control"
                                            placeholder="house/flat no/town"
                                        />
                                        <label htmlFor="floatingDob">Address Line 1</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setAddressLine2(e.target.value);
                                            }}
                                            type="texts"
                                            className="form-control"
                                            placeholder="city/tal/dist/landmark"
                                        />
                                        <label htmlFor="floatingHireDate">Address Line 2</label>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-floating mb-3">
                                        <input
                                            onChange={(e) => {
                                                setZipCode(e.target.value);
                                            }}
                                            type="number"
                                            className="form-control"
                                            placeholder="pincode"
                                        />
                                        <label htmlFor="floatingHireDate">Zip Code</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button
                        onClick={save}
                        className="btn btn-primary me-md-2"
                        type="button"
                    >
                        Save
                    </button>
                    <button onClick={cancel} type="button" className="btn btn-danger">
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    );
}
else {
    return (
        <div>
        <div>
            <GuestHome />
        </div>
        <div>
        <h3 className="title">You Have Already Applied</h3>
        </div>
        </div>
    );
}
};

export default FillApplicationForm
