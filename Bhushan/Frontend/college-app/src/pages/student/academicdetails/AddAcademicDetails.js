import { useState } from 'react'
import { toast } from 'react-toastify'
import { URL } from '../../../config'
import axios from 'axios'
import { useNavigate } from 'react-router'
const AddAcademicDetails=()=>{
    const [sscScore, setSscScore] = useState('')
    const [hscScore, setHscScore] = useState('')
    const [diplomaScore, setDiplomaScore] = useState('')
    const { enrollmentNo, firstName, lastName } = sessionStorage
    const navigate = useNavigate()
    console.log('inside academiRecords')
    const addAcademicDetails=()=>{
        const body={
            sscScore,
            hscScore,
            diplomaScore,
            
            
            
        }

        const url=`${URL}/student/addModifiedAcademicDetails/${enrollmentNo}`
        console.log({url})
        axios.post(url, body).then((response) => {
        
            // get the server result
            const result = response.data
            console.log(result)
            if (result['status'] === 'success') {
              toast.success('Welcome to the application')
    
              // get the data sent by server
              const { academiRecords } = result['data']
               console.log({academiRecords}) 
              
            navigate('/academicDetails')
            } else {
              toast.error('Invalid data')
            }
          })
    }

    return (
        <div>
      <div className="container">
        <div className="row">
          <div className="col-2">Menus</div>
          <div className="col">
            <div className="row">
              <div className="col"></div>
              <div className="col">
                <h5>Add Academic Record</h5>
              </div>
              <div className="col"></div>
            </div>

            <div className="row">
              <div className="col">
                <div class="form-floating mb-3">
                  <input
                    type="text"
                    className="form-control"
                    id="floatingInput"
                    onChange={(e) => {
                        setHscScore(e.target.value)
                      }}
                  />
                  <label for="floatingInput">HSC Score</label>
                </div>
              </div>
              <div className="row">
              <div className="col">
                <div class="form-floating mb-3">
                  <input
                    type="text"
                    className="form-control"
                    id="floatingInput"
                    onChange={(e) => {
                        setSscScore(e.target.value)
                      }}
                    />
                  <label for="floatingInput">SSC Score</label>
                </div>
              </div>
              </div>
              <div className="row">
              <div className="col">
                <div class="form-floating mb-3">
                  <input
                    type="text"
                    className="form-control"
                    id="floatingInput"
                    onChange={(e) => {
                        setDiplomaScore(e.target.value)
                      }}
                  />
                  <label for="floatingInput">Diploma Score</label>
                </div>
              </div>
              </div>
              
            </div>           
        </div>
        </div>
        <div className="row">
            <div className="col">
            <button onClick={addAcademicDetails} type="button" class="btn btn-secondary btn-lg">Add</button>
            </div>
        </div>
        </div>
        </div>
    )
    }

export default AddAcademicDetails;