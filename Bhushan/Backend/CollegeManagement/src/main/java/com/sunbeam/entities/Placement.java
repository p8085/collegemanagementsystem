package com.sunbeam.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "placement")
public class Placement {
	@Id
	private int companyId;
	@Column(name = "company_name")
	private String companyName;
	@Column(name = "company_type")
	private String companyType;
	@Column(name = "ssc_score")
	private double sscScore;
	@Column(name = "hsc_score")
	private double hscScore;
	@Column(name = "diploma_score")
	private double diplomaScore;
	@Column(name = "degree_score")
	private double degreeScore;
	private int vacancies;
	private double companyCtc;

	public Placement() {
		super();
	}

	public Placement(int companyId, String companyName, String companyType, double sscScore, double hscScore,
			double diplomaScore, double degreeScore, int vacancies, double companyCtc) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.companyType = companyType;
		this.sscScore = sscScore;
		this.hscScore = hscScore;
		this.diplomaScore = diplomaScore;
		this.degreeScore = degreeScore;
		this.vacancies = vacancies;
		this.companyCtc = companyCtc;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public double getSscScore() {
		return sscScore;
	}

	public void setSscScore(double sscScore) {
		this.sscScore = sscScore;
	}

	public double getHscScore() {
		return hscScore;
	}

	public void setHscScore(double hscScore) {
		this.hscScore = hscScore;
	}

	public double getDiplomaScore() {
		return diplomaScore;
	}

	public void setDiplomaScore(double diplomaScore) {
		this.diplomaScore = diplomaScore;
	}

	public double getDegreeScore() {
		return degreeScore;
	}

	public void setDegreeScore(double degreeScore) {
		this.degreeScore = degreeScore;
	}

	public int getVacancies() {
		return vacancies;
	}

	public void setVacancies(int vacancies) {
		this.vacancies = vacancies;
	}

	public double getCompanyCtc() {
		return companyCtc;
	}

	public void setCompanyCtc(double companyCtc) {
		this.companyCtc = companyCtc;
	}

	@Override
	public String toString() {
		return "Placement [companyId=" + companyId + ", companyName=" + companyName + ", companyType=" + companyType
				+ ", sscScore=" + sscScore + ", hscScore=" + hscScore + ", diplomaScore=" + diplomaScore
				+ ", degreeScore=" + degreeScore + ", vacancies=" + vacancies + ", companyCtc=" + companyCtc + "]";
	}

}
