package com.sunbeam.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;

@Entity
@Table(name = "guest", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class Guest implements Serializable {
	private static final long serialVersionUID = 1L;
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "registration_no")
	private int registrationNo;
	@Email(message = "Invalid Email")
	private String email;
	@Column(length = 1024)
	private String password;
	private String firstName;
	private String lastName;
	@OneToOne(mappedBy = "guestId")
	private AdmissionForm admissionForm;

	public Guest() {
		super();
	}

	public Guest(int registrationNo, String email, String password, String firstName, String lastName,
			AdmissionForm admissionForm) {
		super();
		this.registrationNo = registrationNo;
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.admissionForm = admissionForm;
	}

	public int getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(int registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public AdmissionForm getAdmissionForm() {
		return admissionForm;
	}

	public void setAdmissionForm(AdmissionForm admissionForm) {
		this.admissionForm = admissionForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Guest [registrationNo=" + registrationNo + ", email=" + email + ", password=" + password
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", admissionForm=" + admissionForm + "]";
	}

}