import { useState } from 'react'
import { Link } from 'react-router-dom'
import './index.css'
import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import { URL } from '../../../config'


const Signin = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

const navigate = useNavigate()
 

const signinGuest = () => {
    if(email.length==0){
        toast.warning('Please Enter Email')
    }else if(password.length==0){
        toast.warning('Please Enter Password')
    }else {
        const body = {
            email,
            password,
        }
    
    const url = `${URL}/guest/signin`

    axios.post(url , body).then((response) => {
        const result = response.data
        console.log(result)
        if(result['status'] == 'success') {
            toast.success('Welcome to the application')

            const {registrationNo , firstName , lastName , applicationStatus} = result['data']
            sessionStorage['registrationNo'] = registrationNo
            sessionStorage['firstName'] = firstName
            sessionStorage['lastName'] = lastName
            sessionStorage['applicationStatus'] = applicationStatus
            sessionStorage['loginStatus'] = 1

            navigate('/guesthome')
        }else{
            toast.error('Invalid user name or password')
        }
    })
}
}

    return (
        <div>
          <h1 className="title">Signin</h1>
    
          <div className="row">
            <div className="col"></div>
            <div className="col">
              <div className="form">
                <div className="mb-3">
                  <label htmlFor="" className="label-control">
                    Email address
                  </label>
                  <input
                    onChange={(e) => {
                      setEmail(e.target.value)
                    }}
                    type="text"
                    className="form-control"
                  />
                </div>
    
                <div className="mb-3">
                  <label htmlFor="" className="label-control">
                    Password
                  </label>
                  <input
                    onChange={(e) => {
                      setPassword(e.target.value)
                    }}
                    type="password"
                    className="form-control"
                  />
                </div>
    
                <div className="mb-3">
                  <div>
                    No account yet? <Link to="/signup">Signup here</Link>
                  </div>
                  <button onClick={signinGuest} className="btn btn-primary">
                    Signin
                  </button>
                </div>
              </div>
            </div>
            <div className="col"></div>
          </div>
        </div>
      )
}

export default Signin