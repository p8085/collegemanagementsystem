package com.sunbeam.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.sunbeam.component.EntityDTOConverter;
import com.sunbeam.dao.GuestDao;
import com.sunbeam.dto.guest.Credentials;
import com.sunbeam.dto.guest.GuestSignupDTO;
import com.sunbeam.entities.Guest;

@Transactional
@Service
public class GuestServiceImpl {
	
	@Autowired
	private GuestDao guestDao;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private EntityDTOConverter converter;

	public GuestSignupDTO saveGuest(GuestSignupDTO guestSignupDto) {
		String rawPassword = guestSignupDto.getPassword();
		String encPassword = passwordEncoder.encode(rawPassword);
		guestSignupDto.setPassword(encPassword);
		Guest guest = converter.toGuestEntity(guestSignupDto);
		guest = guestDao.save(guest);
		guestSignupDto = converter.toGuestDto(guest);
		guestSignupDto.setPassword("*********");
		return guestSignupDto;
	}

	public GuestSignupDTO findUserByEmailAndPassword(Credentials cred) {
		Guest guest = guestDao.findByEmail(cred.getEmail());
		String rawPassword = cred.getPassword();
		if(guest != null && passwordEncoder.matches(rawPassword, guest.getPassword())) {
			GuestSignupDTO guestSignupDto = converter.toGuestDto(guest);
			guestSignupDto.setPassword("********");
			return guestSignupDto;
		}
		return null;
	}
	
}
