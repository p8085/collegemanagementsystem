package com.sunbeam.dto.admissionform;

import org.springframework.stereotype.Component;

@Component
public class AdmissionFormDTO {

	private int formNo;
	private String firstName;
	private String middleName;
	private String lastName;
	private double sscScore;
	private double hscScore;
	private double diplomaScore;
	private double cetScore;
	private String addressLine1;
	private String addressLine2;

	public AdmissionFormDTO() {
		super();
	}

	public AdmissionFormDTO(int formNo, String firstName, String middleName, String lastName, double sscScore,
			double hscScore, double diplomaScore, double cetScore, String addressLine1, String addressLine2) {
		super();
		this.formNo = formNo;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.sscScore = sscScore;
		this.hscScore = hscScore;
		this.diplomaScore = diplomaScore;
		this.cetScore = cetScore;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
	}

	public int getFormNo() {
		return formNo;
	}

	public void setFormNo(int formNo) {
		this.formNo = formNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getSscScore() {
		return sscScore;
	}

	public void setSscScore(double sscScore) {
		this.sscScore = sscScore;
	}

	public double getHscScore() {
		return hscScore;
	}

	public void setHscScore(double hscScore) {
		this.hscScore = hscScore;
	}

	public double getDiplomaScore() {
		return diplomaScore;
	}

	public void setDiplomaScore(double diplomaScore) {
		this.diplomaScore = diplomaScore;
	}

	public double getCetScore() {
		return cetScore;
	}

	public void setCetScore(double cetScore) {
		this.cetScore = cetScore;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	@Override
	public String toString() {
		return "AdmissionFormDTO [formNo=" + formNo + ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", sscScore=" + sscScore + ", hscScore=" + hscScore + ", diplomaScore="
				+ diplomaScore + ", cetScore=" + cetScore + ", addressLine1=" + addressLine1 + ", addressLine2="
				+ addressLine2 + "]";
	}

}
