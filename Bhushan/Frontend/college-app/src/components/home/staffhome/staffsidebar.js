import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as AiOutline from 'react-icons/ai';
import { BsFilterSquare } from "react-icons/bs";

export const staffSidebar = [
    {
      title: ' Staff Detaills',
      path: '/staff/staffdetails',
      icon: <AiOutline.AiOutlineUser/>,
      cName: 'nav-text'
    },
    {
      title: ' Add Staff ',
      path: '/admin/staff/addstaff',
      icon: <BsFilterSquare/>,
      cName: 'nav-text'
    },
    {
        title: 'ChangePassword',
        path: '/changePassword',
        icon: <FaIcons.FaEnvelopeOpenText />,
        cName: 'nav-text'
    },
    {
        title: 'Logout',
        path: '/logout',
        icon: <AiOutline.AiOutlineLogout />,
        cName: 'nav-text'
    },
    {
        title: 'FAQ',
        path: '/faq',
        icon: <IoIcons.IoMdHelpCircle />,
        cName: 'nav-text'
    }
  ];
  